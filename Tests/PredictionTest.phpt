<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 25.5.2015
     * Time: 13:43
     * Package: normaweb
     * Licence: proprietary
     */


    /**
     * @testCase
     *
     */
    use Rampus\Norma\BaseProxy;
    use Rampus\Norma\EntityManager;
    use Rampus\Norma\InvalidPropertyException;
    use Tester\Assert;
    use Tester\TestCase;

    require_once "bootstrap.php";

    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 20.4.2015
     * Time: 9:22
     * Package: normaweb
     * Licence: GPL
     */
    class PredictionTest extends Tester\TestCase
    {
        /** @var \Nette\DI\Container context */
        public $context = null;
        /** @var EntityManager */
        public $em = null;
        /** @var  Context */
        public $db;
        /** @var MockDriver */
        public $driver;

        /**
         * EntityManager_test constructor.
         * @param null $context
         */
        public function __construct($context)
        {
            $this->context = $context;
            $this->em = new EntityManager($context, $this->context->getService('application.application'));
            $this->driver = $this->context->getService('MockDriver');
            $this->ndb = $this->context->getService('database.default.context');
        }


        public function testPrediction()
        {
            $this->driver->reset();
            $en = $this->em->getOne("Test_User", 1);
            $en1 = $this->em->getOne("Test_User", 3);
            $a = $en->name;
            $a = $en->mainDepartment->name;
            $b = $en1->name;
            $b = $en1->mainDepartment->name;
            //Tester\Environment::skip('Prediction skipped');
            Assert::equal(2, $this->driver->counters['GET']);

        }
    }

    $test = new PredictionTest($container);
    $test->run();
