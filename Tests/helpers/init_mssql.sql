USE [test]


IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_departments_lock]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[departments] DROP CONSTRAINT [DF_departments_lock]
END
USE [test]
/****** Object:  Table [dbo].[departments]    Script Date: 09/21/2016 15:47:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[departments]') AND type in (N'U'))
DROP TABLE [dbo].[departments]
USE [test]
/****** Object:  Table [dbo].[departments]    Script Date: 09/21/2016 15:47:18 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[departments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[lock] [int] NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[departments] ADD  CONSTRAINT [DF_departments_lock]  DEFAULT ((0)) FOR [lock]
SET IDENTITY_INSERT [test].[dbo].[departments] ON
INSERT INTO [test].[dbo].[departments]
           ([id],[name])
     VALUES
           (10, 'IT'),
           (20, 'Účtárna'),
           (30, 'Dílna')
SET IDENTITY_INSERT [test].[dbo].[departments] OFF


USE [test]
/****** Object:  Table [dbo].[users]    Script Date: 09/21/2016 16:12:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type in (N'U'))
DROP TABLE [dbo].[users]
USE [test]
/****** Object:  Table [dbo].[users]    Script Date: 09/21/2016 16:12:32 ******/

CREATE TABLE [dbo].[users](
	[ida] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[pass] [varchar](50) NULL,
	[main_departments_id] [int] NULL,
	[f_users_id] [int] NULL,
	[salary] [int] NULL,
	[test] [varchar](50) NULL
) ON [PRIMARY]
SET IDENTITY_INSERT [test].[dbo].[users] ON

INSERT INTO [test].[dbo].[users]
           ([ida]
           ,[name]
           ,[pass]
           ,[main_departments_id]
           ,[f_users_id]
           ,[salary])
     VALUES
            (1,	'Jenda','123456',10,1,10000),
            (2,	'Franta','123456',10,1,15000),
            (3,	'Pepa','123456',30,1,15000),
            (4,	'Jana','123456',20,1,20000),
            (5,	'Hanka','123456',20,1,20000),
            (6,	'Béda','123456',30,1,15000),
            (7,	'Čenda','123456',30,1,10000)
SET IDENTITY_INSERT [test].[dbo].[users] OFF

USE [test]
/****** Object:  Table [dbo].[1_users]    Script Date: 09/21/2016 16:12:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1_users]') AND type in (N'U'))
DROP TABLE [dbo].[1_users]
USE [test]
/****** Object:  Table [dbo].[1_users]    Script Date: 09/21/2016 16:12:32 ******/

CREATE TABLE [dbo].[1_users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[pass] [varchar](100) NULL,
	[main_departments_id] [int] NULL,
	[salary] [int] NULL,
	[test] [varchar](100) NULL
) ON [PRIMARY]
SET IDENTITY_INSERT [test].[dbo].[1_users] ON

INSERT INTO [test].[dbo].[1_users]
           ([id]
           ,[name]
           ,[pass]
           ,[main_departments_id]
           ,[salary])
     VALUES
        (1,	'Prefixed','654321',10,10000)
SET IDENTITY_INSERT [test].[dbo].[1_users] OFF
