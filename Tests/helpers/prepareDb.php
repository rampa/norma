<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 26.5.2015
     * Time: 10:42
     * Package: normaweb
     * Licence: proprietary
     */
    require_once "bootstrap.php";
    $db = $container->getService('database.default.context');

    $db->query(file_get_contents('helpers/init_mysql.sql'));
