SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';


DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `lock` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `departments` (`id`, `name`) VALUES
(10, 'IT'),
(20, 'Účtárna'),
(30, 'Dílna');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ida` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `pass` char(20) NOT NULL,
  `main_departments_id` int(4) NULL,
  `f_users_id` int(4) NULL,
  `salary` int(4) NOT NULL,
  `test` char(100) NULL,
  PRIMARY KEY (`ida`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`ida`, `name`, `pass`,`main_departments_id`,`f_users_id`,`salary`) VALUES
(1,	'Jenda',	123456,10,1,10000),
(2,	'Franta',	123456,10,1,15000),
(3,	'Pepa',	123456,30,1,15000),
(4,	'Jana',	123456,20,1,20000),
(5,	'Hanka',	123456,20,1,20000),
(6,	'Béda',	123456,30,1,15000),
(7,	'Čenda',	123456,30,1,10000);

DROP TABLE IF EXISTS `counters`;
CREATE TABLE `tmp`(
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `value` int(4) NOT NULL,
  `users_id` int(4) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `1_users`;
CREATE TABLE `1_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `pass` char(20) NOT NULL,
  `main_departments_id` int(4) NULL,
  `salary` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `1_users` (`id`, `name`, `pass`,`main_departments_id`,`salary`) VALUES
(1,	'Prefixed',	654321,10,10000);
