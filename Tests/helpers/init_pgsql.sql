SET CLIENT_ENCODING TO 'UTF8';



DROP TABLE IF EXISTS public.departments;

DROP SEQUENCE IF EXISTS public.departments_id_seq;

CREATE SEQUENCE public.departments_id_seq
INCREMENT 1
START 4
MINVALUE 1
MAXVALUE 9223372036854775807
CACHE 1;

ALTER SEQUENCE public.departments_id_seq
OWNER TO honza;
CREATE TABLE public.departments
(
  id integer NOT NULL DEFAULT nextval('departments_id_seq'::regclass),
  name character varying(50) NOT NULL,
  lock integer
)
WITH (
OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.departments
  OWNER to honza;

INSERT INTO departments (id, name) VALUES
  (10, 'IT'),
  (20, 'Účtárna'),
  (30, 'Dílna');

DROP TABLE IF EXISTS public.users;
DROP SEQUENCE IF EXISTS  public.users_id_seq;

CREATE TABLE public.users
(
  ida  SERIAL PRIMARY KEY,
  name character varying(100),
  pass character varying(100),
  main_departments_id integer,
  f_users_id integer,
  salary integer,
  test character varying(100)

)
WITH (
OIDS = TRUE
)
TABLESPACE pg_default;

ALTER TABLE public.users
  OWNER to honza;

INSERT INTO users (name, pass,main_departments_id,f_users_id,salary) VALUES
  (	'Jenda',	123456,10,1,10000),
  (	'Franta',	123456,10,1,15000),
  (	'Pepa',	123456,30,1,15000),
  (	'Jana',	123456,20,1,20000),
  (	'Hanka',	123456,20,1,20000),
  (	'Béda',	123456,30,1,15000),
  (	'Čenda',	123456,30,1,10000);

DROP TABLE IF EXISTS public."1_users";
DROP SEQUENCE IF EXISTS public."1_users_id_seq";

CREATE SEQUENCE public."1_users_id_seq"
INCREMENT 1
START 2
MINVALUE 1
MAXVALUE 9223372036854775807
CACHE 1;

CREATE TABLE public."1_users"
(
  id integer NOT NULL DEFAULT nextval('1_users_id_seq'::regclass),
  name character varying(100),
  pass character varying(100),
  main_departments_id integer,
  f_users_id integer,
  salary integer,
  test character varying(100),
  CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
OIDS = TRUE
)
TABLESPACE pg_default;

ALTER TABLE public.users
  OWNER to honza;

INSERT INTO "1_users" (id, name, pass,main_departments_id,salary) VALUES
  (1,	'Prefixed',	654321,10,10000);
