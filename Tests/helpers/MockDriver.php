<?php
    use Rampus\Norma\DataProvider;
    use Rampus\Norma\DbRequest;
    use Rampus\Norma\DbResult;
    use Rampus\Norma\IDBDriver;

    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 25.5.2015
     * Time: 13:21
     * Package: normaweb
     * Licence: proprietary
     */
    class MockDriver implements IDBDriver
    {
        private $driver;
        public $counters;
        private static $file;

        public function reset()
        {
            $this->counters = [
                'GET' => 0,
                'PUT' => 0,
                'DEL' => 0,
                'FNC' => 0
            ];
        }

        public function __construct($dsn, $name, $password)
        {
            $this->driver = new \Rampus\Norma\MSSQLDriver($dsn, $name, $password);
            //$this->driver = new \Rampus\Norma\MySQLDriver($dsn, $name, $password);
            //$this->driver = new \Rampus\Norma\PgSQLDriver($dsn, $name, $password);
            $this->reset();
            if (!self::$file) {
                self::$file = fopen('SQL_LOG.txt', 'w');
            }
        }

        public function setDataProvider(DataProvider $provider)
        {
            $this->driver->setDataProvider($provider);
        }

        /**
         * @param DbRequest $request
         * @return DbResult
         */
        public function get(DbRequest $request)
        {
            $this->counters['GET']++;

            $res = $this->driver->get($request);
            fwrite(self::$file, $res->getSql() . PHP_EOL);

            return $res;
        }

        public function put(DbRequest $request)
        {
            $this->counters['PUT']++;

            $res = $this->driver->put($request);
            fwrite(self::$file, $res->getSql() . PHP_EOL);

            return $res;
        }

        public function del(DbRequest $request)
        {
            $this->counters['DEL']++;

            $res = $this->driver->del($request);
            fwrite(self::$file, $res->getSql() . PHP_EOL);

            return $res;

        }

        public function fnc(DbRequest $request)
        {
            $this->counters['FNC']++;

            $res = $this->driver->fnc($request);
            fwrite(self::$file, $res->getSql() . PHP_EOL);

            return $res;

        }

        public function prepareDB(array $definition)
        {
            // TODO: Implement prepareDB() method.
        }


        function __destruct()
        {

            //fclose(self::$file);
        }


    }
