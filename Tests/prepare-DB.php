<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 28.5.2015
     * Time: 11:07
     * Package: nORMa
     * Licence: GNU 3
     */

    require_once __DIR__ . "/../../vendor/autoload.php";
    Tester\Helpers::purge(__DIR__ . '/temp/cache');
    $configurator = new Nette\Configurator;
    $configurator->setDebugMode(false);

    $configurator->setTempDirectory(__DIR__ . '/temp');

    $configurator->createRobotLoader()
                 ->addDirectory(__DIR__ . '/../src')
                 ->addDirectory(__DIR__)
                 ->register();
    $configurator->addConfig(__DIR__ . '/helpers/config_mssql.neon');
    $container = $configurator->createContainer();


    echo 'Initializing DB...' . PHP_EOL;
    /** @var \Nette\Database\Context $db */
    $db = $container->getService('database.default.context');

    $a = $db->getConnection()
            ->getPdo()
            ->exec(file_get_contents('helpers/init_mssql.sql'));
    Tester\Helpers::purge(__DIR__ . '/temp/cache');
    echo 'Done.' . PHP_EOL;

