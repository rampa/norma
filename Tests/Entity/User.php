<?php
    /**
     * User: Jan Červený
     * Date: 9.1.14
     * Time: 8:18
     */

    namespace Entity;


    /**
     * Class User
     * @table   users
     * @driver  MockDriver
     *
     * @package Model\Entity
     * @property int    $id
     * @property string $full_name
     * @property string $email
     * @property string $registered
     */
    class Test_User
    {
        /**
         * @var int
         * @primary
         * @column ida
         */
        public $idu;
        public $name;
        public $pass;
        /** @var  Test_Department
         * @entity
         */
        public $mainDepartment;
        private $salary = 10000;
        protected $test;
        /**
         * @var UserPrefixed
         * @entity
         */
        public $fUser;

        public function getFullName()
        {
            return $this->name . ' ' . $this->mainDepartment->name;
        }

        public function setTest($val)
        {
            if (!$val) {
                $this->test = null;

                return;
            }
            if (!$this->test) {
                $this->setTest($val);
                $this->test = $this->test . " test";
            }
        }

    }
