<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 20.4.2015
     * Time: 14:10
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Entity;

    /**
     * Class Test_Department
     * @author Jan Červeny <cerveny@redsoft.cz>
     * @package Entity
     * @table departments
     * @secured
     * @driver  MockDriver
     */
    class Test_Department
    {
        public $name;
        /**
         * @var Test_User
         * @collection
         * @column main_departments_id
         */
        public $employers;
    }
