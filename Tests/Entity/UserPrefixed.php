<?php
    /**
     * User: Jan Červený
     * Date: 9.1.14
     * Time: 8:18
     */

    namespace Entity;


    /**
     * Class User
     * @table users
     * @driver Prefixed
     *
     * @package Model\Entity
     * @property int    $id
     * @property string $full_name
     * @property string $email
     * @property string $registered
     */
    class UserPrefixed
    {
        public $name;
        public $pass;

        private $salary = 10000;

        public function getFullName()
        {
            return $this->name . ' ' . $this->mainDepartment->name;
        }

    }
