<?php
    use Rampus\Norma\EntityManager;
    use Tester\Assert;
    use Tester\TestCase;

    include "bootstrap.php";

    /**
     * @testCase
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 26.5.2015
     * Time: 10:24
     * Package: normaweb
     * Licence: proprietary
     */
    class SelectTest extends TestCase

    {
        /** @var \Nette\DI\Container context */
        public $context = null;
        /** @var EntityManager */
        public $em = null;
        /** @var  Context */
        public $db;
        /** @var MockDriver */
        public $driver;

        /**
         * EntityManager_test constructor.
         * @param null $context
         */
        public function __construct($context)
        {
            $this->context = $context;
            $this->em = $this->context->getService('EntityManager');
            $this->driver = $this->context->getService('MockDriver');
            $this->ndb = $this->context->getService('database.default.context');
        }

        public function testSimple()
        {
            $en = $this->em->getOne('Test_User', 2);
            Assert::same('Franta', $en->name);
            $en = $this->em->getOne('Test_User', ['name' => 'Jana']);
            Assert::same(4, $en->getIdu());
            Assert::same(4, $en->getPrimaryKey());

            //Assert::same(3, $en->getId());
        }

        public function testAND()
        {
            $en = $this->em->getAll('Test_User')
                           ->where(['mainDepartment' => 30])
                           ->where(['idu <' => 7]);
            Assert::equal(2, (int)$en->count('*'));
        }

        public function testIN()
        {
            $en = $this->em->getAll('Test_User')
                           ->where(['idu' => [1, 2]]);
            Assert::equal(2, (int)$en->count('*'));
        }

        public function testOR()
        {
            $en = $this->em->getAll('Test_User')
                           ->where([['mainDepartment' => 30], ['mainDepartment' => 20]]);
            Assert::equal(5, (int)$en->count());
        }

        public function testDots()
        {
            $en = $this->em->getOne('Test_User', ['mainDepartment.name' => 'Dílna']);
            Assert::equal('Pepa', $en->name);
            Assert::exception(function () use ($en) {
                $en = $this->em->getOne('Test_User', ['fUser.name' => 'Prefixed']);
            }, 'Rampus\Norma\InvalidOperationException');
        }

        public function testLike()
        {
            $en = $this->em->getOne('Test_User', ['name LIKE' => '%ank%']);
            Assert::equal('Hanka', $en->name);
        }
    }

    $test = new SelectTest($container);
    $test->run();
