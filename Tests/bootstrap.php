<?php
    /**     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 20.4.2015
     * Time: 9:20
     * Package: normaweb
     * Licence: proprietary
     */


    use Tracy\Debugger;

    require_once __DIR__ . "/../../vendor/autoload.php";
    Debugger::$productionMode = true;

    Tester\Environment::setup();
    //Tester\Helpers::purge(__DIR__ . '/temp/NormaProxy');


    $configurator = new Nette\Configurator;
    $configurator->setDebugMode(false);

    $configurator->setTempDirectory(__DIR__ . '/temp');

    $configurator->createRobotLoader()
                 ->addDirectory(__DIR__ . '/../src')
                 ->addDirectory(__DIR__)
                 ->register();
    //$configurator->addConfig(__DIR__ . '/helpers/config_mssql.neon');
    $configurator->addConfig(__DIR__ . '/helpers/config_mssql.neon');
    $container = $configurator->createContainer();
    return $container;
