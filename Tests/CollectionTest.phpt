<?php
    /**
     * @testCase
     */
    require_once "bootstrap.php";

    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 25.5.2015
     * Time: 14:47
     * Package: normaweb
     * Licence: proprietary
     */
    use Rampus\Norma\EntityManager;
    use Tester\Assert;
    use Tester\TestCase;

    class CollectionTest extends TestCase
    {
        /** @var \Nette\DI\Container context */
        public $context = null;
        /** @var EntityManager */
        public $em = null;
        /** @var  Context */
        public $db;
        /** @var MockDriver */
        public $driver;

        /**
         * EntityManager_test constructor.
         * @param null $context
         */
        public function __construct($context)
        {
            $this->context = $context;
            $this->em = $this->context->getService('EntityManager');
            $this->driver = $this->context->getService('MockDriver');
            $this->ndb = $this->context->getService('database.default.context');
        }

        public function testCreate()
        {
            $collection = $this->em->getAll('Test_User');
            Assert::type('\Rampus\Norma\EntityCollection', $collection);
        }

        public function testGetersSetters()
        {
            $collection = $this->em->getAll('Test_User')
                                   ->where(['idu <' => 3]);
            Assert::equal([1 => "Jenda", 2 => "Franta"], $collection->getAssoc("name"));

        }

        public function testFilters()
        {
            $collection = $this->em->getAll('Test_User')
                                   ->where(['idu <' => 8]);
            Assert::equal(1, $collection->getFirst()
                                        ->getIdu());
            Assert::equal(7, $collection->getLast()
                                        ->getIdu());
            $tst = $collection->find(['idu<' => 5]);
            Assert::equal(4, count($tst));
            Assert::equal(2, $tst[2]->getIdu());
            Assert::equal(2, count($collection->find(['salary<' => 15000])));
            Assert::equal(5, count($collection->find(['salary>=' => 15000])));


        }

        public function testAggregate()
        {
            $collection = $this->em->getAll('Test_User')
                                   ->where(['idu <=' => 7]);
            Assert::equal(10000, (int)$collection->min('salary'));
            Assert::equal(20000, (int)$collection->max('salary'));
            Assert::equal(15000.0, (float)$collection->avg('salary'));
            Assert::equal(7, count($collection));

        }

        public function testDbAggregate()
        {
            $collection = $this->em->getAll('Test_User')
                                   ->where(['idu <=' => 7]);
            Assert::equal(10000, (int)$collection->aggrDb('min', 'salary'));
            Assert::equal(20000, (int)$collection->aggrDb('max', 'salary'));
            Assert::equal(40000, (int)$collection->aggrDb('max', 'salary+salary'));
            Assert::equal(15000.0, (float)$collection->aggrDb('avg', 'salary'));
            Assert::equal(7, (int)$collection->aggrDb('count', '*'));

        }


    }

    $test = new CollectionTest($container);
    $test->run();
