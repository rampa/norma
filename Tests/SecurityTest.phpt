<?php
    use Rampus\Norma\EntityManager;
    use Tester\Assert;
    use Tester\TestCase;

    include "bootstrap.php";

    /**
     * @testCase
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 26.5.2015
     * Time: 10:24
     * Package: normaweb
     * Licence: proprietary
     */
    class SecurityTest extends TestCase

    {
        /** @var \Nette\DI\Container context */
        public $context = null;
        /** @var EntityManager */
        public $em = null;
        /** @var  Context */
        public $db;
        /** @var MockDriver */
        public $driver;

        /**
         * EntityManager_test constructor.
         * @param null $context
         */
        public function __construct($context)
        {
            $this->context = $context;
            $this->em = $this->context->getService('EntityManager');
            $this->driver = $this->context->getService('MockDriver');
            $this->ndb = $this->context->getService('database.default.context');
        }

        public function testLock()
        {
            $en = $this->em->getOne('Test_Department', 20);
            $en->lock(true);
            Assert::exception(function () use ($en) {
                $en->name = 'aaa';
            }, 'Rampus\Norma\EntitySecurityException');
            Assert::exception(function () use ($en) {
                $en->save();
            }, 'Rampus\Norma\EntitySecurityException');
            Assert::exception(function () use ($en) {
                $en->delete();
            }, 'Rampus\Norma\EntitySecurityException');
            Assert::exception(function () use ($en) {
                $en->fromArray(['name' => 1]);
            }, 'Rampus\Norma\EntitySecurityException');
            $en->lock(false);
            Assert::exception(function () use ($en) {
                $en->lock = 2;
            }, 'Rampus\Norma\EntitySecurityException');

        }

    }

    $test = new SecurityTest($container);
    $test->run();
