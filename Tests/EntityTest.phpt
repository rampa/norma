<?php

    /**
     * @testCase
     */
    use Rampus\Norma\EntityManager;
    use Tester\Assert;

    require_once "bootstrap.php";

    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 20.4.2015
     * Time: 9:22
     * Package: normaweb
     * Licence: GPL
     */
    class EntityTest extends Tester\TestCase
    {
        /** @var \Nette\DI\Container context */
        public $context = null;
        /** @var EntityManager */
        public $em = null;
        /** @var  Context */
        public $db;
        /** @var MockDriver */
        public $driver;

        /**
         * EntityManager_test constructor.
         * @param null $context
         */
        public function __construct($context)
        {
            $this->context = $context;
            $this->em = $this->context->getService('EntityManager');
            $this->driver = $this->context->getService('MockDriver');
            $this->ndb = $this->context->getService('database.default.context');
        }

        function testNewEntity()
        {
            $en = $this->em->getOne("Test_User");
            Assert::type("\\Rampus\\Norma\\BaseProxy", $en);
            Assert::type("\\Rampus\\Norma\\BaseProxy", $en->mainDepartment);
            Assert::exception(function () use ($en) {
                $en->getNeexistujiciProperty();
            }, 'Rampus\Norma\InvalidPropertyException');
            Assert::null($en->getIdu());
            Assert::false($en->_isInStore());
            Assert::equal(EntityManager::ENTITY_NEW, $en->_getStatus());
        }

        public function testNotExistingEntity()
        {
            $en = $this->em->getOne("Test_User", 100);
            Assert::type("\\Rampus\\Norma\\BaseProxy", $en);
            Assert::equal(EntityManager::ENTITY_PERSIST, $en->_getStatus());
            Assert::exception(function () use ($en) {
                $a = $en->name;
            }, 'Rampus\Norma\NotFoundException');
            Assert::false($en->_isInStore());
            Assert::equal(EntityManager::ENTITY_NOT_FOUND, $en->_getStatus());
        }

        public function testExistingEntity()
        {
            $this->driver->reset();
            $en = $this->em->getOne("Test_User", 1);
            Assert::type("\\Rampus\\Norma\\BaseProxy", $en);
            Assert::equal(0, $this->driver->counters['GET']);
            Assert::equal(EntityManager::ENTITY_PERSIST, $en->_getStatus());
            $a = $en->name;

            Assert::equal(1, $this->driver->counters['GET']);
            Assert::equal(EntityManager::ENTITY_LOADED, $en->_getStatus());
            Assert::true($en->_isInStore());
        }

        public function testSubEntity()
        {
            $en = $this->em->getOne("Test_User", 1);
            $dep = $en->mainDepartment;
            Assert::type("\\Rampus\\Norma\\BaseProxy", $dep);
            Assert::equal(10, $dep->getId());
            //Assert::equal(EntityManager::ENTITY_PERSIST, $dep->_getStatus());
            Assert::equal('IT', $dep->name);
            Assert::equal(EntityManager::ENTITY_LOADED, $dep->_getStatus());
            Assert::true($en->_isInStore());

        }

        public function testPersistEntity()
        {
            $en = $this->em->getOne("Test_User");
            Assert::equal(EntityManager::ENTITY_NEW, $en->_getStatus());
            $en->name = 'Karel';
            $en->pass = '123456';
            $en->save();
            Assert::true($en->_isInStore());
            Assert::equal(EntityManager::ENTITY_LOADED, $en->_getStatus());
            Assert::equal(8, $en->getIdu());
            $en1 = $this->em->getOne('Test_User', 3);
            Assert::equal('Pepa', $en1->name);
        }

        public function testSave()
        {
            $this->driver->reset();
            $en = $this->em->getOne("Test_User", 7);
            $en->pass = 'heslo';
            $this->em->flush();
            Assert::equal(0, $this->driver->counters['GET']);
            Assert::equal(1, $this->driver->counters['PUT']);

            $en1 = $this->em->getOne('Test_User', ['pass' => 'heslo']);
            Assert::equal(1, $this->driver->counters['GET']);
            Assert::equal('heslo', $en1->pass);
        }

        public function testDeleteEntity()
        {
            $en = $this->em->getOne("Test_User");
            Assert::equal(EntityManager::ENTITY_NEW, $en->_getStatus());
            $en->name = 'Honza';
            $en->pass = '123456';
            $en->save();
            $tmp = $en->idu;
            $en->delete();
            Assert::equal(EntityManager::ENTITY_DELETED, $en->_getStatus());
            Assert::false($en->_isInStore());
            $this->em->flush();
            Assert::equal(1, $this->driver->counters['DEL']);
            $en = $this->em->getOne("Test_User", $tmp);
            Assert::false($en->_isInStore());

        }

        public function testGetSetters()
        {
            $en = $this->em->getOne('Test_User');
            $en->fromArray($this->em->getOne('Test_User', 1)
                                    ->toArray());
            $en->save();
            Assert::equal('Jenda IT', $en->getFullName());
            Assert::exception(function () use ($en) {
                $en->idu = 50;
            }, 'Rampus\Norma\InvalidOperationException');
            Assert::exception(function () use ($en) {
                $en->setIdu(50);
            }, 'Rampus\Norma\InvalidOperationException');
            Assert::exception(function () use ($en) {
                $en->notFoundProperty = 123;
            }, 'Rampus\Norma\InvalidPropertyException');
            $en->test = 'Pepa';
            Assert::same('Pepa test', $en->getTest());
            Assert::same($en->test, $en->getTest());
        }

        public function testArray()
        {
            $arr = [
                'idu'   => 1, 'name' => 'Jenda', 'pass' => '123456', 'mainDepartment' => 10, 'salary' => '10000',
                'fUser' => 1, 'test' => ''
            ];
            $en = $this->em->getOne('Test_User', 1);
            Assert::type('array', $en->toArray());
            Assert::type('int', $en->toArray()['mainDepartment']);
            Assert::type('\Rampus\Norma\BaseProxy', $en->toArray(false)['mainDepartment']);
            Assert::equal($arr, $en->toArray());
            $en1 = $this->em->getOne('Test_User');
            $en1->fromArray($arr);
            Assert::equal($arr['name'], $en1->name);
            Assert::notEqual($arr['idu'], $en1->idu);

        }

        public function testCollection()
        {
            $en = $this->em->getOne('Test_Department', 30);
            Assert::equal(3, (int)$en->employers->count());
        }

        public function testClone()
        {
            $en = $this->em->getOne('Test_User', 1);
            $clone = clone($en);
            Assert::same($en->name, $clone->name);
            Assert::null($clone->getIdu());
            Assert::false($clone->_isInStore());
        }

        public function testTablePrefix()
        {
            $en = $en = $this->em->getOne('UserPrefixed', 1);
            Assert::same($en->name, 'Prefixed');
        }


    }

    $test = new EntityTest($container);
    $test->run();
