<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 13:48
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    class BaseProxy implements \ArrayAccess, \Iterator
    {
        protected $_status = EntityManager::ENTITY_NEW;
        /** @var  EntityManager */
        protected $_em;
        protected $_changed = [];
        /** @var  DataProvider */
        protected $_dataProvider;
        protected $_collections = [];

        /**
         * BaseProxy constructor.
         * @param           $_em
         * @param int       $id
         * @param IDBDriver $_dataProvider
         */
        public function __construct($id, $_em, $_dataProvider)
        {
            $this->_em = $_em;
            $this->_data[$this->getPrimaryKeyName()] = $id;
            $this->_dataProvider = $_dataProvider;
            if ($id) {
                $this->_status = EntityManager::ENTITY_PERSIST;
            }
        }

        /**
         * Return primary key name
         * @return string
         */
        public function getPrimaryKey()
        {
            return $this->_data[$this->getPrimaryKeyName()];
        }

        public function save()
        {
            $this->onSave();
            $request = new DbRequest();
            $request->setOperation('PUT')
                    ->setEntity($this->_getShortName());
            $data = [];
            if ($this->_data[$this->getPrimaryKeyName()]) {
                foreach ($this->_changed as $key => $val) {
                    $data[$key] = $this->_data[$key];
                    $request->setData($data);
                }
                $request->setCondition([$this->getPrimaryKeyName() => $this->_data[$this->getPrimaryKeyName()]]);
                $this->_dataProvider
                    ->execRequest($request);
            } else {
                foreach ($this as $key => $val) {
                    if (!empty($val)) {
                        $data[$key] = $val;
                    }
                }
                $request->setData($data);
                $res = $this->_dataProvider
                    ->execRequest($request);
                $this->_data[$this->getPrimaryKeyName()] = (int)$res->getData();
                $this->_em->registerEntity($this, $this->getPrimaryKey());
                $this->_status = EntityManager::ENTITY_LOADED;

            }
            $this->_changed = [];

            return $this;
        }

        public function onSave()
        {

        }

        final public function _getShortName()
        {
            $reflect = new \ReflectionClass($this);

            return $reflect->getShortName();
        }

        public function delete()
        {
            $this->onDelete();
            $this->_status = EntityManager::ENTITY_DELETED;
            $this->_em->registerDelete($this);
        }

        public function onDelete()
        {

        }

        public function toArray($raw = true)
        {
            $res = [];
            foreach ($this->_data as $key => $valtmp) {
                $val = call_user_func([$this, 'get' . ucfirst($key)]);
                if ((bool)$raw && is_object($val) && is_subclass_of($val, "\\Rampus\\Norma\\BaseProxy")) {
                    $res[$key] = $val->getPrimaryKey();
                } else {
                    $res[$key] = $val;
                }
            }

            return $res;
        }

        public function fromArray($data, $strict = false)
        {
            foreach ($data as $key => $val) {
                if ($key === $this->getPrimaryKeyName()) {
                    continue;
                }
                if (array_key_exists($key, $this->_data)) {
                    if (is_array($val)) {
                        $tmp = $val[$this->getPrimaryKeyName()];
                    } elseif (is_object($val) && is_subclass_of($val, "\\Rampus\\Norma\\BaseProxy")) {
                        $tmp = $val->getId();
                    } else {
                        $tmp = $val;
                    }
                    call_user_func([$this, 'set' . ucfirst($key)], $tmp);
                } elseif ($strict) {
                    throw new InvalidPropertyException("Propert '$key' not found'");
                }
            }

            return $this;
        }

        public function _isInStore()
        {
            if ($this->_getStatus() == EntityManager::ENTITY_PERSIST) {
                try {
                    $this->prepareData();
                } catch (NotFoundException $e) {
                    return false;
                }
            }

            return (bool)($this->_getStatus() & 2);
        }

        /**
         * @return int
         */
        final public function _getStatus()
        {
            return $this->_status;
        }

        final protected function prepareData()
        {
            if ($this->_status === EntityManager::ENTITY_PERSIST) {

                $res =
                    $this->_dataProvider->getEntityData($this->_getShortName(), $this->_data[$this->getPrimaryKeyName()]);
                if (!$res) {
                    $this->_status = EntityManager::ENTITY_NOT_FOUND;
                } else {
                    foreach ($res as $key => $val) {
                        //if (!array_key_exists($key, $this->_changed)) {
                        if (!isset($this->_changed[$key])) { // test speed boost
                            $this->_data[$key] = $val;
                        }
                    }
                    $this->_status = EntityManager::ENTITY_LOADED;
                    $this->onLoad();
                }
            }
            if ($this->_status === EntityManager::ENTITY_NOT_FOUND ||
                $this->_status === EntityManager::ENTITY_DELETED
            ) {
                throw new NotFoundException("Entity not found or deleted");
            }
        }

        public function onLoad()
        {

        }

        final  public function __get($property)
        {
            $fn = 'get' . ucfirst($property);

            return $this->$fn();
        }

        public function __set($property, $value)
        {
            $fn = 'set' . ucfirst($property);

            return $this->$fn($value);
        }

        /**
         * @param      $fn
         * @param null $value
         * @return mixed
         * @throws InvalidOperationException
         * @throws InvalidPropertyException
         * @throws NotFoundException
         */
        public function __call($fn, $value = null)
        {
            $match = [];
            if (preg_match("/^(?P<fn>[a-z]+)(?P<property>[A-Z]\S+)/", $fn, $match)) {
                $property = lcfirst($match['property']);
                if (!array_key_exists($property, $this->_data)) {
                    throw new InvalidPropertyException ("Property '$property' not found");
                }
                switch ($match['fn']) {
                    case 'get':
                        if (!array_key_exists($property, $this->_changed)) {
                            $this->prepareData();
                        }

                        return $this->_data[$property];
                        break;
                    case 'set':
                        if ($value != $this->_data[$property]) {
                            $this->beforeUpdate($property, $value[0]);
                            $this->_data[$property] = $value[0];
                            if ($this->_status & EntityManager::ENTITY_PERSIST && $this->_em->registerChange($this)) {
                                $this->_changed[$property] = true;
                            } else {
                                unset($this->_changed[$property]);
                            }
                            $this->afterUpdate($property, $value[0]);
                        }
                        break;
                    case
                    'is':
                        if (!array_key_exists($property, $this->_changed)) {
                            $this->prepareData();
                        }

                        return (bool)$this->_data[$property];
                        break;
                    default:
                        throw new InvalidOperationException ("Function $fn  not found");

                }
            } else {
                throw new InvalidOperationException ("Function $fn  not found");
            }
        }

        public function beforeUpdate($property, $value)
        {

        }

        public function afterUpdate($property, $value)
        {

        }

        /**
         * @experimental
         */
        public function __clone()
        {
            $this->_status = EntityManager::ENTITY_NEW;
            $this->_data[$this->getPrimaryKeyName()] = null;
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Whether a offset exists
         * @link http://php.net/manual/en/arrayaccess.offsetexists.php
         * @param mixed $offset <p>
         *                      An offset to check for.
         *                      </p>
         * @return boolean true on success or false on failure.
         *                      </p>
         *                      <p>
         *                      The return value will be casted to boolean if non-boolean was returned.
         */
        public function offsetExists($offset)
        {
            return array_key_exists($offset, $this->_data);
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Offset to retrieve
         * @link http://php.net/manual/en/arrayaccess.offsetget.php
         * @param mixed $offset <p>
         *                      The offset to retrieve.
         *                      </p>
         * @return mixed Can return all value types.
         */
        public function offsetGet($offset)
        {
            $fn = 'get' . ucfirst($offset);

            return $this->$fn();
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Offset to set
         * @link http://php.net/manual/en/arrayaccess.offsetset.php
         * @param mixed $offset <p>
         *                      The offset to assign the value to.
         *                      </p>
         * @param mixed $value  <p>
         *                      The value to set.
         *                      </p>
         * @return void
         */
        public function offsetSet($offset, $value)
        {
            $fn = 'set' . ucfirst($offset);

            return $this->$fn($value);
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Offset to unset
         * @link http://php.net/manual/en/arrayaccess.offsetunset.php
         * @param mixed $offset <p>
         *                      The offset to unset.
         *                      </p>
         * @throws \Rampus\Norma\InvalidOperationException
         */
        public function offsetUnset($offset)
        {
            throw new InvalidOperationException('Can not unset property');
        }

        public function __isset($name)
        {
            return isset($this->_data[$name]);
        }

        public function current()
        {
            $this->prepareData();

            return current($this->_data);
        }

        public function next()
        {
            return next($this->_data);
        }

        public function key()
        {
            return key($this->_data);
        }

        public function valid()
        {
            return key($this->_data) !== null;

        }

        public function rewind()
        {
            reset($this->_data);
        }


    }
