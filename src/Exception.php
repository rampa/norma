<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 13:48
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;

    class Exception extends \Exception
    {

    }

    class InvalidPropertyException extends Exception
    {

    }

    class InvalidValueException extends Exception
    {

    }

    class InvalidOperationException extends Exception
    {

    }

    class EntityDefinitionNotFound extends Exception
    {

    }

    class NotFoundException extends Exception
    {

    }

    class EntityDeletedException extends Exception
    {

    }

    class InvalidEntityDefinitionException extends Exception
    {

    }

    class EntitySecurityException extends Exception
    {

    }
