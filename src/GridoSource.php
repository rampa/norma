<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 13:48
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;

    use Grido\DataSources\IDataSource;

    /**
     * Class GridoSource
     *
     * @description Data source for use with o5 Grido
     * @author      Jan Červeny <cerveny@redsoft.cz>
     * @package     Rampus\Norma
     */
    class GridoSource implements IDataSource
    {
        /** @var  EntityCollection */
        private $collection;

        /**
         * Contructor
         *
         * @param EntityCollection $collection
         */
        public function __construct(EntityCollection $collection) {
            $this->collection = $collection;
        }

        /**
         * Return count of rows
         *
         * @return int
         */
        function getCount() {
            $a=$this->collection->aggrDb('count');
            return (int)$a;
        }

        /**
         * Return data from collection
         *
         * @return array
         */
        function getData() {
            return $this->collection;
        }

        /**
         * Implement filtering
         *
         * @param array $condition
         *
         * @return void
         */
        function filter(array $condition) {
            /** @var \Grido\Components\Filters\Condition $val */
            foreach ($condition as $val) {
                $column = $val->getColumn();
                $cond = $val->getCondition();
                $value = $val->getValue();
                if ($cond[0] == "IS NOT NULL") {
                    $this->collection->where([$column[0] . ' IS NOT' => null]);
                } elseif ($cond[0] == "IS NULL") {
                    $this->collection->where([$column[0] . ' IS' => null]);
                } elseif ($cond[0] == "LIKE ?") {
                    $this->collection->where([$column[0] . ' LIKE' => $value[0]]);
                }elseif($cond[0] == "= ?") {
                    $this->collection->where([$column[0] => $value[0]]);
                }
            }
        }

        /**
         * Set limit and offset
         *
         * @param int $offset
         * @param int $limit
         *
         * @return void
         */
        function limit($offset, $limit) {
            $this->collection->limit( $limit,$offset);
        }

        /**
         * Implement sorting
         *
         * @param array $sorting
         *
         * @return void
         */
        function sort(array $sorting) {
            $this->collection->order(key($sorting) . ' ' . current($sorting));
        }

        /**
         * Suggest for filtering
         *
         * @param mixed $column
         * @param array $conditions
         * @param int   $limit
         *
         * @return array
         */
        function suggest($column, array $conditions, $limit) {
            sleep(1);
            return ['a','b'];
            // TODO: Implement suggest() method.
        }
    }
