<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 20.8.2015
     * Time: 13:56
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    use Nette\Object;

    class AggrResult extends Object
    {
        public $id;
        public $value;
    }
