<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 19.5.2015
     * Time: 13:35
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    use Tracy\Debugger;

    /**
     * Class PgSQLDriver
     * @author  Jan Červeny <cerveny@redsoft.cz>
     * @package Rampus\Norma
     */
    class PgSQLDriver implements IDBDriver
    {
        private static $paramGen = 0;
        /** @var  \PDO */
        private $PDO;
        /** @var  DataProvider */
        private $dataProvider;
        private $definitions = [];
        private $types       = [
            'varchar' => []
        ];
        private $prefix      = null;


        /**
         * MySQLDriver constructor.
         */
        public function __construct($dsn, $name, $password)
        {
            $this->PDO = new \PDO($dsn, $name, $password);
            $this->PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->PDO->query("SET CLIENT_ENCODING TO 'UTF8';");
        }

        public function setDataProvider(DataProvider $provider)
        {
            $this->dataProvider = $provider;
        }

        /**
         * @param DbRequest $request
         * @return DbResult
         */
        public function get(DbRequest $request)
        {
            $res = new DbResult();
            $query = 'SELECT ';
            $tmp = [];
            $def = $this->getDefinition($request->getEntity());
            foreach ($def['columns'] as $key => $val) {
                $tmp[] =
                    $this->getTableName($request) . "." . $this->quote($val['column']) . ' AS ' . $this->quote($key);
            }
            $query .= implode(", ", $tmp);
            $query .= ' FROM ' . $this->getTableName($request);
            $where = $this->addWhere($request);
            $query .= $this->addJoin($request);
            $query .= $where;
            $query .= $this->addOrder($request);
            $query .= $this->addLimit($request);
            Debugger::timer('MySQLDriver');
            $stmt = $this->PDO->prepare($query);
            $stmt->execute($request->getParameters());
            $data = [];
            foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $val) {
                $data[(int)$val[$this->getDefinition($request->getEntity())['primaryKey']]] =
                    $this->normalizeData($val, $def);
            }
            $res->setTime(Debugger::timer('MySQLDriver'))
                ->setData($data)
                ->setSql($query)
                ->setRows($stmt->rowCount());

            return $res;

        }

        private function getDefinition($entity)
        {
            if (!isset($this->definitions[$entity])) {
                $this->definitions[$entity] = $this->dataProvider->getDefinition($entity);
            }

            return $this->definitions[$entity];
        }

        /**
         * @param $request
         * @return string
         */
        private function getTableName($request)
        {
            return $this->quote(($this->getDefinition($request->getEntity())['schema']) ? $this->getDefinition($request->getEntity())['schema'] : 'public') .
                   "." .
                   $this->quote($this->prefix . $this->getDefinition($request->getEntity())['table']);
        }

        private function quote(string $item)
        {
            return '"' . $item . '"';
        }

        private function addWhere(DbRequest $request)
        {
            if ($request->getCondition()) {
                $cond = $this->parseCondition($request->getCondition(), $request);

                return (' WHERE ' . $cond);
            }

            return '';

        }

        private function parseCondition($cond, DbRequest $request)
        {
            $res = "";
            foreach ($cond as $key => $val) {
                if (is_int($key)) {
                    $res = (strlen($res) ? '(' . $res . ') OR (' : null) . $this->parseCondition($val, $request) .
                           (strlen($res) ? ')' : null);
                } elseif (is_array($val)) {
                    $res .= (strlen($res) ? ' AND ' : null) . $this->mapE2DB($key, $request) . " IN (";
                    foreach ($val as $tmp) {
                        $p = $this->getParamNum();
                        $res .= $p . ',';
                        $request->addParameter($p, $tmp);
                    }
                    $res = substr($res, 0, -1);
                    $res .= ")";

                } else {
                    $tmp = explode(' ', $key);
                    $key = $tmp[0];
                    unset($tmp[0]);
                    $operator = implode(' ', $tmp);
                    $operator = $operator ? strtoupper(' ' . $operator . ' ') : '=';
                    $res .= (strlen($res) ? ' AND ' : null);
                    $p = $this->getParamNum();
                    $res .= '' . $this->mapE2DB($key, $request) . $operator . (($val === '.EXP.') ? '' : $p);
                    if ($val !== '.EXP.') {
                        $request->addParameter($p, $val);
                    }
                }
            }

            //$request->setCondition($res);
            return $res;

        }

        private
        function mapE2DB($column, DbRequest $request)
        {
            $parsed = explode(".", $column);
            if (count($parsed) > 1) {
                $join = $this->getDefinition($request->getEntity())['columns'][$parsed[0]]['type'];
                if ($this->getDefinition($join)['driver'] != $this->getDefinition($request->getEntity())['driver']) {
                    throw new InvalidOperationException("Both entity must have the same driver in case dot syntax - " .
                                                        implode(".", $parsed));
                }
                $request->addJoin(
                    $this->quote(($this->getDefinition($join)['schema']) ? $this->getDefinition($join)['schema'] : 'public') .
                    '.' .
                    $this->quote($this->getPrefix() . $this->getDefinition($join)['table']) . ' AS ' .
                    $this->quote($parsed[0]),
                    $this->quote($parsed[0]) . '."id"=' . $this->getTableName($request) . '.'
                    . $this->quote($this->getDefinition($request->getEntity())['columns'][$parsed[0]]['column']));

                return $this->quote($parsed[0]) . '.' .
                       $this->quote($this->getDefinition($join)['columns'][$parsed[1]]['column']);
            }
            if (array_key_exists($parsed[0], $this->getDefinition($request->getEntity())['columns'])) {
                return $this->getTableName($request) . '.'
                       . $this->quote($this->getDefinition($request->getEntity())['columns'][$column]['column']);
            } else {
                return $this->quote($parsed[0]);
            }
        }

        /**
         * @return null
         */
        public function getPrefix()
        {
            return $this->prefix;
        }


//---------------------------------------------------------------------------------------- helpers ---------------------

        /**
         * @param null $prefix
         * @return MySQLDriver
         */
        public function setPrefix($prefix)
        {
            $this->prefix = $prefix;

            return $this;
        }

        private function getParamNum()
        {
            return ':' . ++self::$paramGen;
        }

        private function addJoin(DbRequest $request)
        {
            $out = '';
            if ($request->getJoins()) {
                foreach ($request->getJoins() as $key => $val) {
                    $out .= ' JOIN ' . $key . ' ON ' . $val;
                }
            }

            return $out;

        }

        private function addOrder(DbRequest $request)
        {
            if (count($request->getOrder())) {
                $ordr = $request->getOrder();
                array_walk($ordr, function (&$val) use ($request) {
                    $tmp = explode(" ", $val);
                    $tmp[0] = $this->mapE2DB($tmp[0], $request);
                    $val = implode(" ", $tmp);
                });

                return ' ORDER BY ' . implode(", ", $ordr);
            }

            return '';
        }

        private function addLimit(DbRequest $request)
        {
            $out = '';
            if ($request->getLimit()[0]) {
                $out .= ' LIMIT ' . $request->getLimit()[0];
                if ($request->getLimit()[1]) {
                    $out .= ' OFFSET ' . $request->getLimit()[1];
                }
            }

            return $out;
        }

        private function normalizeData($data, $def)
        {
            $res = [];
            foreach ($data as $key => $val) {
                if ($def['columns'][$key]['entity']) {
                    $res[$key] = (int)$val;
                } else {
                    $res[$key] = Normalizer::normalize($def['columns'][$key]['type'], $val);
                }
            }

            return $res;
        }

        public function put(DbRequest $request)
        {
            if ($request->getCondition()) {
                $where = $this->addWhere($request);
                $query = 'UPDATE ' . $this->getTableName($request);
                $query .= $this->addJoin($request);
                $query .= ' SET ';
                $data = [];
                \Tracy\Debugger::barDump($request->getData());

                foreach ($request->getData() as $key => $val) {
                    $p = $this->getParamNum();
                    $data[] = $col[] =
                        $this->quote($this->getDefinition($request->getEntity())['columns'][$key]['column']) . '=' . $p;
                    $request->addParameter($p, $val);
                }
                $query .= implode(",", $data);
                $query .= $where;
                $query .= $this->addOrder($request);
                $query .= $this->addLimit($request);
                Debugger::timer('MySQLDriver');
                $stmt = $this->PDO->prepare($query);
                $stmt->execute($request->getParameters());
                $result = new DbResult();
                $result->setTime(Debugger::timer('MySQLDriver'))
                       ->setSql($query)
                       ->setRows($stmt->rowCount());

                return $result;
            } else {
                $query = "INSERT INTO " . $this->getTableName($request) . ' (';
                $col = [];
                $values = [];
                foreach ($request->getData() as $key => $val) {
                    $col[] = $this->quote($this->getDefinition($request->getEntity())['columns'][$key]['column']);
                    $p = $this->getParamNum();
                    $values[] = $p;
                    $request->addParameter($p, $val);
                }
                $query .= implode(",", $col) . ") VALUES (" . implode(",", $values) . ') RETURNING id;';
                Debugger::timer('MySQLDriver');
                $stmt = $this->PDO->prepare($query);
                $stmt->execute($request->getParameters());
                //$query = "SELECT currval(pg_get_serial_sequence('" . $this->getTableName($request) . "','id'));";
                //$stmt->execute();
                $result = new DbResult();
                $result->setRows($stmt->rowCount())
                       ->setData($stmt->fetch()['id'])
                       ->setTime(Debugger::timer('MySQLDriver'))
                       ->setSql($query);

                return $result;
            }
        }

        public function del(DbRequest $request)
        {
            $query = "DELETE FROM " . $this->getTableName($request);
            $where = $this->addWhere($request);
            $query .= $this->addJoin($request);
            $query .= $where;
            $query .= $this->addOrder($request);
            $query .= $this->addLimit($request);
            Debugger::timer('MySQLDriver');
            $stmt = $this->PDO->prepare($query);
            $stmt->execute($request->getParameters());
            $res = new DbResult();
            $res->setTime(Debugger::timer('MySQLDriver'))
                ->setSql($query)
                ->setRows($stmt->rowCount());

            return $res;
        }

        public function fnc(DbRequest $request)
        {
            $param = $request->getData();
            $param[1] = preg_replace_callback("/(\w+)+/", function ($matches) use ($request) {
                return $this->mapE2DB($matches[0], $request);
            }, $param[1]);
            $fnc = $param[0] . "(" . $param[1] . ")";
            $query = "SELECT " . $param[0] . "(" . $param[1] . ")";
            $query .= " FROM " . $this->getTableName($request);
            $where = $this->addWhere($request);
            $query .= $this->addJoin($request);
            $query .= $where;
            $query .= $this->addOrder($request);
            $query .= $this->addLimit($request);
            Debugger::timer('MySQLDriver');
            $stmt = $this->PDO->prepare($query);
            $stmt->execute($request->getParameters());
            $res = new DbResult();
            $data = $stmt->fetch();
            $res->setTime(Debugger::timer('MySQLDriver'))
                ->setRows($stmt->rowCount())
                ->setData(count($data) ? $data[0] : null)
                ->setSql($query);

            return $res;

        }

        public function prepareDB(array $definition)
        {
            $rows = [];
            $table = $this->getPrefix() . $definition['table'];
            $smtp = $this->PDO->prepare('SHOW COLUMNS FROM ' . $table);
            try {
                $smtp->execute();
                $cols = $smtp->fetchAll(\PDO::FETCH_COLUMN | \PDO::FETCH_GROUP);
                foreach ($definition['columns'] as $key => $val) {
                    if (isset($cols[$val['column']])) {
                        $type = $val['type'] . ($val['size'] ? "(" . $val['size'] . ")" : null);
                        if ($cols[$val['column']][0] != $type) {
                            $rows[] = "CHANGE `" . $val['column'] . "` " . $this->makeRow($key, $val);
                        }
                    } else {
                        $rows[] = "ADD " . $this->makeRow($key, $val);
                    }
                }
                if (count($rows)) {
                    $this->PDO->query("ALTER TABLE $table " . implode(",", $rows) . ";");
                }

            } catch (\PDOException $e) {
                $err = $smtp->errorCode();
                if ($err != "42S02") {
                    throw $e;
                }
                $this->createTable($definition);

                return;
            }


        }

        private function makeRow($key, $val)
        {
            $row = "`" . $val['column'] . "` ";
            if ($key == "id") {
                $row .= " int(4) NOT NULL AUTO_INCREMENT";
            } elseif ($val['entity']) {
                $row .= "int(4) NULL";
            } else {
                $row .= $this->convertType($val['type']) . ($val['size'] ? "(" . $val['size'] . ')' : "") . ' NULL';
            }

            return $row;

        }

        private function convertType($type)
        {
            $conv = ['string' => 'varchar'];

            return isset($conv[$type]) ? $conv[$type] : $type;
        }

        private function createTable($def)
        {
            $rows = [];
            $query = "CREATE TABLE `" . $this->getPrefix() . $def['table'] . "` (";
            foreach ($def['columns'] as $key => $val) {
                $rows[] = $this->makeRow($key, $val);
            }
            $query .= implode(",", $rows) . ", PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $this->PDO->query($query);
        }

    }

