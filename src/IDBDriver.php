<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 13:48
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    use Nette\ComponentModel\Container;

    interface IDBDriver
    {
        public function setDataProvider(DataProvider $provider);

        /**
         * @param DbRequest $request
         * @return DbResult
         */
        public function get(DbRequest $request);

        public function put(DbRequest $request);

        public function del(DbRequest $request);

        public function fnc(DbRequest $request);

        public function prepareDB(array $definition);


    }
