<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 11:39
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    use Nette\DI\Container;
    use Tracy\Debugger;

    class DataProvider extends \Nette\Object
    {
        public  $onQuery     = [];
        private $dataStore   = [];
        private $definitions = [];
        private $drivers     = [];
        private $debugBar    = null;
        /** @var  Container */
        private $context;
        private $queue = [];
        /** @var  EntityManager */
        private $em;
        private $collectionCache = [];

        public function __construct(Container $context, $em)
        {
            $this->context = $context;
            if (!Debugger::$productionMode) {
                $panel = new NormaPanel();
                $this->debugBar = $panel;
                $this->onQuery[] = [$panel, 'logQuery'];
            }
            $this->em = $em;
        }

        public function getEntityData($entity, $id)
        {
            $res = $this->getFromStore($entity, $id);
            if (!$res) {
                $cond = array_unique([$id] + $this->getQueued($entity));
                $request = new DbRequest();
                $request->setEntity($entity)
                        ->setOperation('GET')
                        ->setCondition([$this->getDefinition($entity)['primaryKey'] => $cond])
                        ->setLimit(count($cond));
                $this->execRequest($request);

                return $this->getFromStore($entity, $id);
            }

            return $res;
        }

        private function getFromStore($entity, $id)
        {
            if (!array_key_exists($entity, $this->dataStore) || !array_key_exists($id, $this->dataStore[$entity])) {
                return false;
            }
            $res = $this->dataStore[$entity][$id];
            unset($this->dataStore[$entity][$id]);

            return $res;
        }

        private function getQueued($entity)
        {
            if (array_key_exists($entity, $this->queue)) {
                $res = $this->queue[$entity];
                unset($this->queue[$entity]);

                return $res;
            }

            return [];
        }

        public function getDefinition($entity)
        {
            if (!array_key_exists($entity, $this->definitions)) {
                $this->em->getOne($entity);
            }

            return $this->definitions[$entity];
        }

        public function execRequest(DbRequest $request)
        {
            $request->setParameters([]);
            switch (strtoupper($request->getOperation())) {
                case 'GET':
                    $res = $this->getDriver($request->getEntity())
                                ->get($request);
                    $cols = $this->getDefinition($request->getEntity())['columns'];
                    foreach ($res->getData() as $val) {
                        $this->addDataToStore($request->getEntity(), $val[$this->getDefinition($request->getEntity())['primaryKey']], $val);
                        foreach ($val as $eKey => $eVal) {
                            if ($eVal && $cols[$eKey]['entity']) {
                                $this->addToQueue($cols[$eKey]['type'], $eVal);
                            }
                        }
                    }
                    break;
                case 'PUT':
                    $res = $this->getDriver($request->getEntity())
                                ->put($request);
                    break;
                case 'DEL':
                    $res = $this->getDriver($request->getEntity())
                                ->del($request);
                    break;
                    break;
                default:
                    $request->setOperation('FNC');
                    $res = $this->getDriver($request->getEntity())
                                ->fnc($request);
                    break;

            }

            $this->onQuery($request, $res);

            return $res;
        }

        /**
         * @param string $entity
         * @return IDBDriver
         */
        private function getDriver($entity)
        {
            $service = $this->definitions[$entity]['driver'];
            if (!isset($this->drivers[$service])) {
                if ($service == 'default') {
                    $this->getDefaultDriver();
                    //$this->drivers[$service] = $this->context->getService(array_shift($srvr));
                    //$this->drivers[$service] = array_pop($this->context->findByType("Rampus\\Norma\\IDBDriver"));
                } else {
                    $this->drivers[$service] = $this->context->getService($service);
                }
                $this->drivers[$service]->setDataProvider($this);
            }

            return $this->drivers[$service];
        }

        private function getDefaultDriver()
        {
            foreach (($this->context->findByType("Rampus\\Norma\\IDBDriver")) as $val) {
                if (preg_match("/^\d+_/", $val, $matches)) {
                    $this->drivers['default'] = $this->context->getService($val);

                    return true;
                }
            }
            throw new NotFoundException("Default driver not found");
        }

        private function addDataToStore($entity, $id, $data)
        {
            if (!array_key_exists($entity, $this->dataStore)) {
                $this->dataStore[$entity] = [];
            }
            $this->dataStore[$entity][$id] = $data;
        }

        public function addToQueue($entity, $id)
        {
            if (!array_key_exists($entity, $this->queue)) {
                $this->queue[$entity] = [];
            }
            $this->queue[$entity][] = $id;
        }

        public function getCollection(DbRequest $request)
        {
            $hash = md5(serialize($request));
            if (!isset($this->collectionCache[$hash])) {
                $response = $this->execRequest($request);
                $res = [];
                foreach ($response->getData() as $key => $val) {
                    $res[$val[$this->getDefinition($request->getEntity())['primaryKey']]] =
                        $this->em->getOne($request->getEntity(), $val[$this->getDefinition($request->getEntity())['primaryKey']]);

                }
                $this->collectionCache[$hash] = $res;
            }

            return $this->collectionCache[$hash];

        }

        public function hasDefinition($entity)
        {
            return array_key_exists($entity, $this->definitions);
        }

        public function addDefinition($entity, $def, $rebuild = false)
        {
            $this->definitions[$entity] = $def;
            if ($rebuild) {
                $this->getDriver($entity)
                     ->prepareDB($def);
            }

            return $this;
        }
    }
