<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 13:48
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    use Nette\PhpGenerator\Parameter;
    use Nette\PhpGenerator\Parameter as PHPGenParameter;
    use Nette\PhpGenerator\PhpNamespace;
    use Nette\Reflection\ClassType;
    use Nette\Reflection\Method;

    class Parser
    {
        private static $forbidenFunction = [
            'setId',
            'getId',
            'delete',
            'persist',
            'prepareData',
            'fromArray',
            'toArray',
            '__get',
            '__set',
            '__call'
        ];

        public static function parse($entity)
        {
            $refl = ClassType::from($entity);
            $properties = [];
            $namespace = new PhpNamespace('Entity\Proxy');
            $proxy = $namespace->addClass($refl->getShortName());
            $proxy->setExtends('\Rampus\Norma\BaseProxy');

            $definition = [
                'table'       => $refl->hasAnnotation('table')
                    ? $refl->getAnnotation('table')
                    : strtolower((preg_replace('/(\w+)([A-Z])/', '$1_$2', $refl->getShortName())) . 's'),
                'schema'      => $refl->hasAnnotation('schema') ? $refl->getAnnotation('schema') : '',
                'driver'      => $refl->hasAnnotation('driver') ? $refl->getAnnotation('driver') : 'default',
                'entities'    => [],
                'columns'     => [],
                'collections' => [],
                'primaryKey'  => 'id'
            ];
            $primaryKey = self::findPrimaryKey($refl);
            $primaryKeyName = $primaryKey['name'];

            unset($primaryKey['name']);

            $columns[$primaryKeyName] = $primaryKey;
            $definition['primaryKey'] = $primaryKeyName;
            $properties[$definition['primaryKey']] = null;
            $aaa=$refl->getProperties();
            foreach ($refl->getProperties() as $property) {
                if ($property->getName() == $primaryKeyName) {
                    continue;
                }
                $tmp = ['entity' => false];

                $match = [];
                if ($property->hasAnnotation('var') &&
                    preg_match("/(?P<type>\w+)\s*(?P<size>\d*)?i?/", $property->getAnnotation('var'), $match)
                ) {
                    $tmp['type'] = $match['type'];
                    $tmp['size'] = isset($match['size']) ? $match['size'] : null;
                } else {
                    $tmp['type'] = "string";
                    $tmp['size'] = 100;
                }

                if ($property->hasAnnotation('entity')) {
                    $tmp['column'] = $property->hasAnnotation('column')
                        ? $property->getAnnotation('column')
                        : (strtolower(preg_replace('/([A-Z])/', '_$1', $property->getName())) . 's_id');
                    $tmp['entity'] = true;
                    $tmp['type'] = $property->getAnnotation('var');
                    $tmp['size'] = 4;


                    self::entityGetterSetter($property->getName(), $tmp['type'], $proxy);
                } elseif ($property->hasAnnotation('collection')) {
                    $tmp['collection'] = true;
                    $tmp['column'] = $property->hasAnnotation('column')
                        ? strtolower($property->getAnnotation('column'))
                        : strtolower($definition['table'] . '_id');
                    $met = $proxy->addMethod('get' . ucfirst($property->getName()));
                    $met->setVisibility('public');
                    $body = '$this->prepareData();'
                            . PHP_EOL;
                    $body .= 'if(!array_key_exists("'
                             . $property->getName() . '",$this->_collections)){'
                             . PHP_EOL;
                    $body .= '$this->_collections["'
                             . $property->getName()
                             . '"]= $this->_em->getAll(\''
                             . $tmp['type'] . '\')'
                             . PHP_EOL;
                    if ($property->hasAnnotation('thru')) {
                        $body .= '->thru(\'' . $property->getAnnotation('thru') . '\',$this);' . PHP_EOL;
                        $body .= '}' . PHP_EOL . 'return $this->_collections["' . $property->getName() . '"];';
                    } else {
                        $body .= '->where([\'' . $tmp['column'] . '\'=>$this->getPrimaryKey()]);' . PHP_EOL;
                        $body .= '}' . PHP_EOL . 'return $this->_collections["' . $property->getName() . '"];';
                        $param = new Parameter();
                        $param->setName('entity')
                              ->setTypeHint(ucfirst($property->getAnnotation('var')));
                        /*
                        $proxy->addMethod('add' . ucfirst($property->getName()))
                              ->setVisibility('public')
                              ->setParameters([$param])
                              ->setBody('$entity->set' . ucfirst($tmp['column']) . '($this);' . PHP_EOL . 'return $this;');
                        */
                    }
                    $met->setBody($body);
                    continue; // do not add property to columns !!!

                } else {
                    $tmp['column'] = $property->hasAnnotation('column')
                        ? $property->getAnnotation('column')
                        : strtolower(preg_replace('/([A-Z])/', '_$1', $property->getName()));
                }
                $property->setAccessible(true);
                //$properties[$property->getName()] = [];
                $properties[$property->getName()] = $property->getValue($entity) ? $property->getValue($entity) : null;
                $columns[$property->getName()] = $tmp;
            };
            if ($refl->hasAnnotation('secured')) {
                $columns['lock'] =
                    ['column' => 'id', 'type' => 'int', 'size' => 4, 'nullable' => false, 'entity' => false];
                $properties['lock'] = null;
                $proxy->setExtends('\Rampus\Norma\BaseSecuredProxy');
            }


            $definition['columns'] = $columns;
            $proxy->addProperty("_data", $properties)
                  ->setVisibility('protected');
            $proxy->addMethod('set' . ucfirst($primaryKeyName))
                  ->setVisibility('public')
                  ->setbody('throw new \Rampus\Norma\InvalidOperationException("Property ID is ReadOnly.");');
            $proxy->addMethod('getPrimaryKeyName')
                  ->setVisibility('public')
                  ->setbody('return "' . $primaryKeyName . '";');
            foreach ($refl->getMethods() as $method) {


                if (isset($proxy->getMethods()[$method->getName()]) ||
                    in_array($method->getName(), self::$forbidenFunction)
                ) {
                    throw new InvalidEntityDefinitionException("Function '{$method->getName()}' is reserved for system. Rename it in '{$refl->getShortName()}' entity");
                }

                $proxy->addMethod($method->getName())
                      ->setParameters(self::getFuncParamatr($method))
                      ->setVisibility('public')
                      ->setBody(self::getFuncBody($method));
            }


            return ['def' => $definition, "proxy" => $namespace];
        }

        private static function findPrimaryKey(ClassType $refl)
        {
            $availDateType = ['int', 'longint', 'smallint', 'tinyint', 'bigint', 'decimal'];
            $hasPrimary = 0;
            $primaryKey = [];
            foreach ($refl->getProperties() as $property) {
                if ($property->hasAnnotation('primary')) {
                    $property->hasAnnotation('var') &&
                    preg_match("/(?P<type>\w+)\s*(?P<size>\d*)?i?/", $property->getAnnotation('var'), $match);
                    $hasPrimary++;
                    $primaryKey['type'] = $match['type'];
                    $primaryKey['size'] = isset($match['size']) ? $match['size'] : 4;
                    $primaryKey['column'] = $property->hasAnnotation('column')
                        ? $property->getAnnotation('column')
                        : strtolower(preg_replace('/([A-Z])/', '_$1', $property->getName()));
                    $primaryKey['nullable'] = false;
                    $primaryKey['entity'] = false;
                    $primaryKey['name'] = $property->getName();
                }

            }
            if (!$hasPrimary) {
                $primaryKey = [
                    'name'   => 'id', 'column' => 'id', 'type' => 'int', 'size' => 4, 'nullable' => false,
                    'entity' => false
                ];
            }
            if ($hasPrimary > 1) {
                throw new InvalidEntityDefinitionException("Multiple primary key detected");
            }

            return $primaryKey;
        }

        private
        static function entityGetterSetter($name, $entity, \Nette\PhpGenerator\ClassType $proxy)
        {
            $method = $proxy->addMethod("get" . ucfirst($name));
            $method->setVisibility('public');
            $method->setBody('$this->prepareData();' . PHP_EOL . 'return $this->_em->getOne("' . ucfirst($entity) .
                             '",$this->_data["' . $name . '"]);');
            $method = $proxy->addMethod("set" . ucfirst($name));
            $method->setVisibility('public');
            $prop = new Parameter();
            $prop->setName('value');
            $method->setParameters([$prop]);
            $body = 'if(((is_numeric($value) || empty($value)) && $value != $this->_data["' . $name .
                    '"])||($value instanceof ' . ucfirst($entity) . ' && $value->getPrimaryKey() != $this->_data["' . $name .
                    '"])){' . PHP_EOL;
            $body .= "\t" . 'if($value instanceof ' . ucfirst($entity) . '){' . PHP_EOL . '$this->_data["' . $name .
                     '"]=$value->getPrimaryKey();' . PHP_EOL;
            $body .= '}else{' . PHP_EOL;
            $body .= '$this->_data["' . $name . '"]=(int)$value;';
            $body .= '}' . PHP_EOL;
            $body .= 'if ($this->_status & \Rampus\Norma\EntityManager::ENTITY_PERSIST && $this->_em->registerChange($this)) {' .
                     PHP_EOL;
            $body .= '  $this->_changed["' . $name . '"] = true;' . PHP_EOL . '}';
            $body .= '}';

            $method->setBody($body);

        }

        private static function getFuncParamatr(Method $func)
        {
            $param = [];
            foreach ($func->getParameters() as $p) {
                $pa = new PHPGenParameter();
                $pa->setName($p->getName());
                $param[] = $pa;
            }

            return $param;
        }

        private static function getFuncBody(Method $func)
        {
            $filename = $func->getFileName();
            $start_line = $func->getStartLine() - 1;
            $end_line = $func->getEndLine();
            $length = $end_line - $start_line;
            $source = file($filename);
            $body = implode(array_slice($source, $start_line, $length));
            $body = substr($body, strpos($body, '{') + 1);
            $body = substr($body, 0, strrpos($body, '}'));

            $property = substr($func->getName(), 3);
            if (substr($func->getName(), 0, 3) == 'get') {
                //$body = preg_replace('/(this->' . lcfirst($property) . ')(\s*)([^=])/x', 'this->__call("get' . $property . '",null)$2', $body);
                $body = preg_replace('/\s*(=?)\s*this->' . lcfirst($property) . '(?!\s*=)/m', 'this->__call("get' .
                                                                                              $property .
                                                                                              '",null)$2', $body);
                $pattern = '/(\$this->set' . ucfirst($property) . ')\((.*)\)/i';
                $pattern = '/(\$this->)([a-z0-9]*)\h*\=(.*);/i';
                $body = preg_replace_callback($pattern, function ($matches) use ($property) {
                    return '$this->set' . ucfirst($matches[2]) . '(' . $matches[3] . ');';
                }, $body);
            }
            if (substr($func->getName(), 0, 3) == 'set') {

                $body = preg_replace('/\s*(=?)\s*this->' . lcfirst($property) . '(?!\s*=)/m', 'this->__call("get' .
                                                                                              $property .
                                                                                              '",null)$2', $body);
                $pattern = '/(\$this->set' . ucfirst($property) . ')\((.*)\)/i';
                $body = preg_replace($pattern, '$this->__call(\'set' . ucfirst($property) . '\',[${2}])', $body);
                $pattern = '/(\$this->)([a-z0-9]*)\h*\=(.*);/i';
                $body = preg_replace_callback($pattern, function ($matches) use ($property) {
                    if ($matches[2] == lcfirst($property)) {
                        return '$this->__call(\'set' . ucfirst($matches[2]) . '\',[' . $matches[3] . ']);';
                    } else {
                        return '$this->set' . ucfirst($matches[2]) . '(' . $matches[3] . ');';
                    }
                }, $body);
            }

            return $body;
        }
    }
