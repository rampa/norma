<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 1.9.2015
     * Time: 12:22
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    class BaseSecuredProxy extends BaseProxy
    {
        final public function lock($val = true)
        {
            $this->_data['lock'] = $val;

            return $this;
        }

        final public function isLocked()
        {
            return $this->_data['lock'];
        }

        public function __call($fn, $val = null)
        {
            $match = [];
            if (preg_match("/^(?P<fn>[a-z]+)(?P<property>[A-Z]\S+)/", $fn, $match)) {
                $property = lcfirst($match['property']);
                if ($match['fn'] == 'set' && $this->isLocked()) {
                    throw new EntitySecurityException("Entity is locked");
                } elseif ($match['fn'] == 'set' && $match['property'] == 'Lock') {
                    throw new EntitySecurityException('Can not write to property lock');
                };
            }

            return parent::__call($fn, $val);
        }

        public function save()
        {
            if ($this->isLocked()) {
                throw new EntitySecurityException("Entity is locked");
            }

            return parent::save();
        }

        public function delete()
        {
            if ($this->isLocked()) {
                throw new EntitySecurityException("Entity is locked");
            }

            return parent::delete();
        }
    }
