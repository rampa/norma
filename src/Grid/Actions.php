<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 7.5.2015
     * Time: 15:12
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    use Nette\ComponentModel\Container;

    class Actions extends Container
    {
        public function renderHead()
        {

        }

        public function renderCell()
        {

        }

        public function getCellPrototype()
        {
            return \Nette\Utils\Html::el('span');
        }
    }
