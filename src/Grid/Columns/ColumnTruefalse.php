<?php
    /**
     * Author: Jan ?erven?
     * Email: cerveny@redsoft.cz
     * Date: 6.5.2015
     * Time: 16:45
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    use Latte\Runtime\Filters;
    use Nette\ComponentModel\IContainer;
    use Nette\Utils\Html;
    use Nette\Utils\Strings;

    class ColumnTrueFalse extends ColumnText
    {
        protected $format;

        public function __construct(Grid $parent = null, $name = null, $label)
        {
            parent::__construct($parent, $name, $label);
            //$this->getCellPrototype()->class .= ' text-right';
            $this->cellRenderer = function ($row) {
                if($this->getNestedValue($row, explode(".", $this->bindedColumn))){
                    return Html::el('i class="red glyphicon glyphicon-ok"');
                }else{
                    return Html::el('i class="green glyphicon glyphicon-remove"');
                }
            };

            return $this;
        }

        /**
         * @return mixed
         */
        public function getFormat()
        {
            return $this->format;
        }

        /**
         * @param mixed $format
         */
        public function setFormat($format)
        {
            $this->format = $format;

            return $this;
        }


    }
