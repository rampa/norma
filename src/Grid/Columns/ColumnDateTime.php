<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 7.5.2015
     * Time: 14:54
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    class ColumnDateTime extends ColumnText
    {
        protected $format = "j.n.Y";

        public function __construct(Grid $parent = null, $name = null, $label)
        {
            parent::__construct($parent, $name, $label);
            $this->getCellPrototype()->class .= ' text-right';
            $this->cellRenderer = function ($row) {
                return $this->getNestedValue($row, explode(".", $this->bindedColumn))
                            ->format($this->format);
            };

            return $this;
        }

        /**
         * @return mixed
         */
        public function getFormat()
        {
            return $this->format;
        }

        /**
         * @param mixed $format
         */
        public function setFormat($format)
        {
            $this->format = $format;

            return $this;
        }

    }
