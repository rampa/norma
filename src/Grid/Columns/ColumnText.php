<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 6.5.2015
     * Time: 10:15
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    use Nette\ComponentModel\Component;
    use Nette\ComponentModel\IContainer;
    use Nette\Utils\Callback;
    use Nette\Utils\Html;

    class ColumnText extends Component
    {
        protected $headerPrototype;
        protected $cellPrototype;
        protected $cellRenderer;
        protected $sumaPrototype;
        protected $headerRenderer;
        protected $headerLabel;
        protected $sortable;
        protected $bindedColumn;

        protected function getNestedValue($ent, $path)
        {
            $res = $ent[array_shift($path)];
            if (count($path)) {
                $res = $this->getNestedValue($res, $path);
            }

            return $res;
        }

        public function __construct(Grid $parent = null, $name = null, $label)
        {
            parent::__construct($parent, $name);
            $this->headerLabel = $label;
            $el = Html::el('th');
            $el->class = mb_strtolower($this->name) . '-head';
            $this->headerPrototype = $el;
            $el = Html::el('td');
            $el->class = mb_strtolower($this->name) . '-cell';
            $this->cellPrototype = $el;
            $this->headerRenderer = function (Html $el) {
                if ($this->sortable) {
                    $sort = $this->getParent()
                                 ->getSort();
                    $a = Html::el('a');
                    $a->setText($this->headerLabel);
                    $a->href = $this->parent->link('sort!', $this->name);
                    if (mb_strpos($sort, $this->name) !== false) {
                        $arrow = Html::el('i class="glyphicon glyphicon-chevron-down"');
                        if (mb_strpos($sort, 'DESC') === false) {
                            $arrow->class = "glyphicon glyphicon-chevron-up";
                            $a->href = $this->parent->link('sort!', ($this->name . ' DESC'));
                        }
                        $a->add($arrow);
                    }
                    $el->add($a);
                } else {
                    $el->setText($this->headerLabel);
                }

                return $el;
            };
            $this->cellRenderer = function ($row) {
                return $this->getNestedValue($row, explode(".", $this->bindedColumn));
            };
            $this->bindedColumn = $name;

            return $this;
        }

        public function setColumn($col)
        {
            $this->bindedColumn = $col;
        }

        /**
         * @return mixed
         */
        public function getSortable()
        {
            return $this->sortable;
        }

        /**
         * @return Column
         */
        public function setSortable()
        {
            $this->sortable = true;

            return $this;
        }


        public function getHeaderPrototype()
        {
            return $this->headerPrototype;

        }

        public function getCellPrototype()
        {
            return $this->cellPrototype;

        }

        public function setCellRenderer($fn)
        {
            if (!Callback::check($fn)) {
                throw new \InvalidArgumentException ('Renderer must be callable object');
            }
            $this->cellRenderer = $fn;

            return $this;
        }

        public function renderHead()
        {
            $el = Callback::invokeArgs($this->headerRenderer, [$this->getHeaderPrototype()]);

            return (string)$el;
        }

        public function renderCell($row)
        {
            $el = Callback::invokeArgs($this->cellRenderer, [$row]);

            return $el;
        }
    }
