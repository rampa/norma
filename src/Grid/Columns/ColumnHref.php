<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 17.6.2015
     * Time: 9:51
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    use Nette\Utils\Html;

    class ColumnHref extends ColumnText
    {
        private $link = null;
        private $ajax = false;
        private $target = null;
        private $column = "id";

        public function __construct(Grid $parent = null, $name = null, $label)
        {
            parent::__construct($parent, $name, $label);
            $this->cellRenderer = function ($row) {
                $el = Html::el('a');
                $el->addAttributes([
                                       'class'  => $this->ajax ? "ajax " : "",
                                       'target' => $this->target ? $this->target : null,
                                       'href'   => $this->parent->parent->link($this->link, $row[$this->column])
                                   ]);

                $el->setHtml($this->getNestedValue($row, explode(".", $this->bindedColumn)));

                return $el;
            };
        }

        /**
         * @return null
         */
        public function getLink()
        {
            return $this->link;
        }

        /**
         * @param null $link
         */
        public function setLink($link, $column = null)
        {
            $this->link = $link;
            if ($column) {
                $this->column = $column;
            }


            return $this;
        }

        /**
         * @return boolean
         */
        public function getAjax()
        {
            return $this->ajax;
        }

        /**
         * @param boolean $ajax
         * @return ColumnHref
         */
        public function setAjax($ajax)
        {
            $this->ajax = $ajax;

            return $this;
        }

        /**
         * @return null
         */
        public function getTarget()
        {
            return $this->target;
        }

        /**
         * @param null $target
         * @return ColumnHref
         */
        public function setTarget($target)
        {
            $this->target = $target;

            return $this;
        }

    }
