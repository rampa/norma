<?php
    /**
     * Author: Jan ?erven?
     * Email: cerveny@redsoft.cz
     * Date: 6.5.2015
     * Time: 16:45
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    use Latte\Runtime\Filters;
    use Nette\ComponentModel\IContainer;
    use Nette\Utils\Strings;

    class ColumnNumber extends ColumnText
    {
        protected $format;

        public function __construct(Grid $parent = null, $name = null, $label)
        {
            parent::__construct($parent, $name, $label);
            $this->getCellPrototype()->class .= ' text-right';
            $this->cellRenderer = function ($row) {
                return $this->getNestedValue($row, explode(".", $this->bindedColumn));
            };

            return $this;
        }

        /**
         * @return mixed
         */
        public function getFormat()
        {
            return $this->format;
        }

        /**
         * @param mixed $format
         */
        public function setFormat($format)
        {
            $this->format = $format;

            return $this;
        }


    }
