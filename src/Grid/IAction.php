<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 7.5.2015
     * Time: 15:16
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    use Nette\ComponentModel\Component;
    use Nette\ComponentModel\IContainer;
    use Nette\Forms\Container;

    interface IAction extends IContainer
    {

    }
