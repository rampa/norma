<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 7.5.2015
     * Time: 15:18
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Norma\Grid;


    use Nette\Application\UI\Presenter;
    use Nette\ComponentModel\Container;
    use Nette\ComponentModel\IContainer;
    use Nette\Utils\Callback;
    use Nette\Utils\Html;

    class Action extends Container implements IAction
    {
        private $link;
        private $label;
        private $prototype;
        private $class = "";
        private $icon = "";
        private $renderer;
        private $ajax = false;

        public function __construct(IContainer $parent = null, $name = null, $link = null)
        {
            parent::__construct($parent, $name);
            $this->link = $link ? $link : $this->getName();
            $this->label=$name;

        }

        /**
         * @return mixed
         */
        public function getLabel()
        {
            return $this->label;
        }

        /**
         * @param mixed $label
         * @return Action
         */
        public function setLabel($label)
        {
            $this->label = $label;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getPrototype()
        {
            if(!$this->prototype) {
                $el = Html::el('a');
                $el->class = "btn btn-xs btn-minier btn-default";
                $el->setText($this->label);
                $this->prototype = $el;
            }
            return $this->prototype;
        }


        /**
         * @return mixed
         */
        public function getLink()
        {
            return $this->link;
        }

        public function setIcon($txt)
        {
                $this->getPrototype()->insert(0,'<i class="' . $txt . '"></i>');

        }

        public function addClass($class)
        {
            $this->class = $class;
            return $this;
        }

        /**
         * @param mixed $link
         * @return Action
         */
        public function setLink($link)
        {
            $this->link = $link;

            return $this;
        }

        public function render($row, $presenter)
        {
            $out = "";
            if ($this->renderer) {
                $out = Callback::invokeArgs($this->renderer, [$row, $this->getPrototype(), $presenter]);
            } else {
                /** @var Html $prototype */
                $prototype = $this->getPrototype();
                $prototype->addAttributes([
                                              'href'  => $presenter->link($this->link, $row['id']),
                                              'class' => $prototype->class . " " . $this->class
                                          ]);
                $out .= $prototype->__toString();

            };

            return $out;
        }
    }


