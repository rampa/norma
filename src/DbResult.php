<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 20.5.2015
     * Time: 9:57
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    class DbResult
    {
        private $sql;
        private $rows;
        private $time;

        /**
         * @return mixed
         */
        public function getTime()
        {
            return $this->time;
        }

        /**
         * @param mixed $time
         * @return DbResult
         */
        public function setTime($time)
        {
            $this->time = $time;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getSql()
        {
            return $this->sql;
        }

        /**
         * @param mixed $sql
         * @return DbResult
         */
        public function setSql($sql)
        {
            $this->sql = $sql;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getRows()
        {
            return $this->rows;
        }

        /**
         * @param mixed $rows
         * @return DbResult
         */
        public function setRows($rows)
        {
            $this->rows = $rows;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getData()
        {
            return $this->data;
        }

        /**
         * @param mixed $data
         * @return DbResult
         */
        public function setData($data)
        {
            $this->data = $data;

            return $this;
        }
        private $data;
    }
