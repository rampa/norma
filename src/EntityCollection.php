<?php

    namespace Rampus\Norma;

    use Nette\MemberAccessException;
    use Nette\Object;

    /**
     * Class EntityCollection
     * @author  Jan Červeny <cerveny@redsoft.cz>
     * @package Rampus\Norma
     * Licence: BSD
     */
    class EntityCollection extends Object implements \Iterator, \ArrayAccess, \Countable
    {
        /** @var array */
        private static $allovedFunction = [
            'MIN', 'MAX', 'AVG', 'COUNT'
        ];
        /** @var int */
        private $_status = EntityManager::ENTITY_NEW;
        /** @var array */
        private $_collection = [];
        /** @var array */
        private $_changed = [];
        /** @var DbRequest */
        private $_request;
        /** @var EntityManager */
        private $_em;
        /** @var DataProvider */
        private $_dataProvider;


        /**
         * EntityCollection constructor.
         * @param               $entity
         * @param EntityManager $em
         * @param DataProvider  $dp
         */
        function __construct($entity, EntityManager $em, DataProvider $dp)
        {
            $this->_em = $em;
            $this->_dataProvider = $dp;
            $this->_request = new DbRequest();
            $this->_request->setEntity($entity);
        }

        /**
         * Add condition
         * @param \Array $cond
         * @return $this
         * @throws InvalidOperationException
         */
        public function where($cond)
        {
            if ($this->_status === EntityManager::ENTITY_NEW) {
                $this->_request->addCondition($cond);
            } else {
                throw new InvalidOperationException("Can not set condition to loaded collection");
            }

            return $this;
        }

        /**
         * Set llimit and offset
         * @param int      $limit
         * @param int|null $offset
         * @return $this
         * @throws \Rampus\Norma\InvalidOperationException
         */
        public function limit($limit, $offset = null)
        {
            if ($this->_status === EntityManager::ENTITY_NEW) {
                $this->_request->setLimit($limit, $offset);
            } else {
                throw new InvalidOperationException("Can not set limit to loaded collection");
            }

            return $this;
        }

        /**
         * Set order
         * @param $order
         * @return $this
         * @throws \Rampus\Norma\InvalidOperationException
         */
        public function order($order)
        {
            if ($this->_status === EntityManager::ENTITY_NEW) {
                $this->_request->setOrder($order);
            } else {
                throw new InvalidOperationException("Can not set limit to loaded collection");
            }

            return $this;
        }

        /**
         * Getdata from db - internal function
         * @internal
         */
        private function prepareData()
        {
            $this->_collection = $res = $this->_dataProvider->getCollection($this->_request->setOperation('GET'));
            $this->_status = EntityManager::ENTITY_LOADED;
        }

        /**
         * Main getter overide
         * @param $property
         * @throws InvalidOperationException
         * @return null
         */
        public function &__get($property)
        {
            throw new InvalidOperationException("Use getAssoc(\$prop). \$collection->property is deprecated.");
        }

        /**
         * Set the property for all entities
         * @param $property
         * @param $value
         */
        public function setAll($property, $value)
        {
            if ($value instanceof BaseProxy) {
                $value = $value->getId();
            }
            $trackChange = $this->_em->getTrackChange();
            $this->_em->setTrackChange(false);
            foreach ($this as $val) {
                $val->$property = $value;
            }
            $this->_changed[$property] = $value;
            $this->_em->setTrackChange($trackChange);
            $this->_em->registerChange($this);

        }

        /**
         * Get assoc array
         * @param array|string ...$arg
         * @return array
         */
        public function getAssoc(...$arg)
        {
            if (!count($arg)) {
                throw new \InvalidArgumentException("Provide at least one argument");
            }
            $res = [];
            foreach ($this as $id => $entity) {
                if (count($arg) > 1) {
                    $tmp = [];
                    foreach ($arg as $val) {
                        $tmp[] = $entity->$val;
                    }
                    $res[$id] = $tmp;
                } else {

                    $res[$id] = $entity->{$arg[0]};
                }
            }

            return $res;
        }

        /**
         * Save all changes to db
         */
        public function save()
        {
            $this->_request->setData($this->_changed)
                           ->setOperation('PUT');
            $this->_dataProvider->execRequest($this->_request);
            $this->_changed = [];
        }

        /**
         * Delete all entities from db
         */
        public function delete()
        {
            foreach($this->_collection as $item){
                $item->onDelete();
            }
            $this->_request->setData($this->_changed)
                           ->setOperation('DEL');
            $this->_dataProvider->execRequest($this->_request);
            $this->_changed = [];
            $this->_collection = [];
        }

        /**
         * Performs aggregation on database
         * @param      $fn
         * @param null $arg
         * @return mixed
         */
        public function aggrDb($fn, $arg = null)
        {
            $request = clone($this->_request);
            if (!$arg) {
                $arg = '*';
            }
            $request->setOperation('FNC');
            $request->setData([$fn, $arg]);

            $res = $this->_dataProvider->execRequest($request)
                                       ->getData();

            //return $this->_em->getOne($this->_request->getEntity(), $res);
            return $res;

        }

        /**
         * Get first entity
         * @return mixed
         */
        public function getFirst()
        {
            $this->rewind();

            return $this->current();
        }

        /**
         * Get last entity
         * @return mixed
         *
         */
        public function getLast()
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                $this->prepareData();
            }
            end($this->_collection);

            return $this->current();
        }

        /**
         * Get first entity by property value
         * @param $property
         * @param $value
         * @return BaseProxy|bool
         */
        public function getBy($property, $value)
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                $this->prepareData();
            }
            foreach ($this->_collection as $val) {
                if ($val->$property == $value) {
                    return $val;
                }
            }

            return false;
        }

        /**
         * Get all entities by parameters
         * example ['id >'=>10]
         * @param array $arg
         * @return array
         */
        public function find($arg)
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                $this->prepareData();
            }
            $res = [];
            $match = [];
            /*   i know easier method, but eval is evil :) */
            if (preg_match('/(\w+).*?([=|<|>]{1,2}$)/si', key($arg), $match)) {
                switch ($match[2]) {
                    case '<':
                        foreach ($this as $key => $val) {
                            if ($val->{$match[1]} < current($arg)) {
                                $res[$key] = $val;
                            }
                        }
                        break;
                    case '>':
                        foreach ($this as $key => $val) {
                            if ($val->{$match[1]} > current($arg)) {
                                $res[$key] = $val;
                            }
                        }
                        break;
                    case '<=':
                        foreach ($this as $key => $val) {
                            if ($val->{$match[1]} <= current($arg)) {
                                $res[$key] = $val;
                            }
                        }
                        break;
                    case '>=':
                        foreach ($this as $key => $val) {
                            if ($val->{$match[1]} >= current($arg)) {
                                $res[$key] = $val;
                            }
                        }
                        break;
                    case '=':
                        foreach ($this as $key => $val) {
                            if ($val->{$match[1]} == current($arg)) {
                                $res[$key] = $val;
                            }
                        }
                        break;
                    case '!=':
                        foreach ($this as $key => $val) {
                            if ($val->{$match[1]} != current($arg)) {
                                $res[$key] = $val;
                            }
                        }
                        break;
                    default:
                        throw new \InvalidArgumentException('Undefined operator in expression: ' . key($arg));

                }

            };

            return $res;
        }

        /**
         * Get minimal value of property from colection
         * @param $property
         * @return mixed
         */
        public function min($property)
        {
            return min($this->getAssoc($property));
        }

        /**
         * Get maximal value of property from collection
         * @param $property
         * @return mixed
         */
        public function max($property)
        {
            return max($this->getAssoc($property));
        }

        /**
         * Get avarage value of property from colection
         * @param $property
         * @return float
         */
        public function avg($property)
        {
            $tmp = $this->getAssoc($property);

            return array_sum($tmp) / count($tmp);
        }

        /**
         * Get suma value of property from colection
         * @param $property
         * @return number
         */
        public function sum($property)
        {
            return array_sum($this->getAssoc($property));
        }
// ----------------------------------------------------------------------------------- implements ----------------------
        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Return the current element
         * @link http://php.net/manual/en/iterator.current.php
         * @return mixed Can return any type.
         */
        public function current()
        {
            while (current($this->_collection) && (current($this->_collection)->_getStatus() == EntityManager::ENTITY_DELETED)) {
                unset($this->collection[key($this->_collection)]);
                //$this->next();
            }

            return current($this->_collection);

        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Move forward to next element
         * @link http://php.net/manual/en/iterator.next.php
         * @return void Any returned value is ignored.
         */
        public function next()
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                $this->prepareData();
            }

            next($this->_collection);
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Return the key of the current element
         * @link http://php.net/manual/en/iterator.key.php
         * @return mixed scalar on success, or null on failure.
         */
        public function key()
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                $this->prepareData();
            }
            while (current($this->_collection) && (current($this->_collection)->_getStatus() == EntityManager::ENTITY_DELETED)) {
                unset($this->_collection[key($this->_collection)]);
                //$this->next();
            }

            return key($this->_collection);
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Checks if current position is valid
         * @link http://php.net/manual/en/iterator.valid.php
         * @return boolean The return value will be casted to boolean and then evaluated.
         * Returns true on success or false on failure.
         */
        public function valid()
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                $this->prepareData();
            }
            while (current($this->_collection) && (current($this->_collection)->_getStatus() == EntityManager::ENTITY_DELETED)) {
                unset($this->_collection[key($this->_collection)]);
                //$this->next();
            }

            return isset($this->_collection[$this->key()]);

        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Rewind the Iterator to the first element
         * @link http://php.net/manual/en/iterator.rewind.php
         * @return void Any returned value is ignored.
         */
        public function rewind()
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                $this->prepareData();
            }
            reset($this->_collection);
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Whether a offset exists
         * @link http://php.net/manual/en/arrayaccess.offsetexists.php
         * @param mixed $offset
         * An offset to check for.
         *
         * @return boolean true on success or false on failure.
         *
         * The return value will be casted to boolean if non-boolean was returned.
         */
        public function offsetExists($offset)
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                return array_key_exists($offset, $this->_collection);
            }
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Offset to retrieve
         * @link http://php.net/manual/en/arrayaccess.offsetget.php
         * @param mixed $offset
         * The offset to retrieve.
         *
         * @return mixed Can return all value types.
         */
        public function offsetGet($offset)
        {
            if ($this->_status == EntityManager::ENTITY_NEW) {
                return $this->_collection[$offset];
            }
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Offset to set
         * @link http://php.net/manual/en/arrayaccess.offsetset.php
         * @param mixed $offset
         *                     The offset to assign the value to.
         *
         * @param mixed $value <p>
         *                     The value to set.
         *                     </p>
         * @return void
         */
        public function offsetSet($offset, $value)
        {
            throw new MemberAccessException('Set offset is not allowed');
        }

        /**
         * (PHP 5 &gt;= 5.0.0)<br/>
         * Offset to unset
         * @link http://php.net/manual/en/arrayaccess.offsetunset.php
         * @param mixed $offset
         * The offset to unset.
         *
         * @return void
         */
        public function offsetUnset($offset)
        {
            throw new MemberAccessException('Unset offset is not allowed');
        }

        /**
         * (PHP 5 &gt;= 5.1.0)<br/>
         * Count elements of an object
         * @link http://php.net/manual/en/countable.count.php
         * @return int The custom count as an integer.
         *
         * The return value is cast to an integer.
         */
        public function count()
        {
            $count = 0;
            foreach ($this as $val) {
                $count++;
            }

            return $count;
        }

    }
