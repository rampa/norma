<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 23.11.2015
     * Time: 14:44
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Rampus\Norma;


    use Nette\Utils\Image;

    class FileDriver implements IDBDriver
    {
        private $dataProvider;
        private $path;

        /**
         * FileDriver constructor.
         * @param $path
         */
        public function __construct($path)
        {
            /*
            $x = $path . "/../images/nette-powered3.gif";
            $im = Image::fromFile($path . "/../images/HDR.jpg");
            $im->save($path . "/avatar/" . md5(1) . ".png", Image::PNG);
            mkdir($path . "/avatar");
*/
            $this->path = $path;
            if (!is_dir($path)) {
                mkdir($path);
            }
        }

        /**
         * @param mixed $path
         */
        public function setPath($path)
        {
            $this->path = $path;
        }

        public function setDataProvider(DataProvider $provider)
        {
            $this->dataProvider = $provider;
        }

        public function get(DbRequest $request)
        {
            $path = $this->path . DIRECTORY_SEPARATOR . $request->getEntity();
            $result = new DbResult();
            if (!is_dir($path)) {
                mkdir($path);
            }
            $id = md5($request->getCondition()['id'][0]);
            $result->setSql("store//" . $request->getEntity() . DIRECTORY_SEPARATOR . ":1");
            $request->addParameter(":1", $id . ".png");
            if (is_file($path . DIRECTORY_SEPARATOR . $id . ".png")) {
                $result->setData([$request->getCondition()['id'][0] => ['id' => $request->getCondition()['id'][0], 'image' => Image::fromFile($path . DIRECTORY_SEPARATOR . $id . ".png")]]);
                $result->setRows(1);
            } else {
                $result->setData([]);
            }

            return $result;
        }

        public function put(DbRequest $request)
        {
            // TODO: Implement put() method.
        }

        public function del(DbRequest $request)
        {
            // TODO: Implement del() method.
        }

        public function fnc(DbRequest $request)
        {
            // TODO: Implement fnc() method.
        }

        public function prepareDB(array $definition)
        {
            // TODO: Implement prepareDB() method.
        }

    }
