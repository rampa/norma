<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 13:48
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    use Nette\Utils\DateTime;

    class Normalizer
    {
        public static $types = [
            'int'      => 'integer',
            'integer'  => 'integer',
            'string'   => 'string',
            'datetime' => 'datetime',
            'float'    => 'float',
        ];

        public static function normalize($type, $value)
        {
            if (isset(self::$types[$type])) {
                //return forward_static_call_array([__NAMESPACE__ . '\Normalizer', self::$types[$type]], [$value]);
                $fn = self::$types[$type];

                return self::$fn($value);

            } else {
                return $value;
            }
        }

        public static function integer($value)
        {
            return (int)$value;
        }

        public static function float($value)
        {
            return (float)$value;
        }

        public static function string($value)
        {
            return (string)$value;
        }

        public static function datetime($value)
        {
			if($value){
				return DateTime::from($value);
			}else{
				return $value;
			}
        }

    }
