<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 19.5.2015
     * Time: 13:27
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    class DbRequest
    {
        private $entity;
        private $operation = 'GET';
        private $condition = [];
        private $parameters = [];
        private $limit = [null, null];
        private $order = [];
        private $joins=[];
        private $data;

        /**
         * @return mixed
         */
        public function getData()
        {
            return $this->data;
        }

        /**
         * @param mixed $data
         * @return DbRequest
         */
        public function setData($data)
        {
            $this->data = $data;

            return $this;
        }


        /**
         * @return string
         */
        public function getOperation()
        {
            return $this->operation;
        }

        /**
         * @param string $operation
         * @return DbRequest
         */
        public function setOperation($operation)
        {
            $this->operation = $operation;

            return $this;
        }
        /**
         * @return array
         */
        public function getJoins()
        {
            return $this->joins;
        }

        /**
         * @param array $joins
         * @return DbRequest
         */
        public function setJoins($joins)
        {
            $this->joins = $joins;

            return $this;
        }

        public function addJoin($table,$on){
            $this->joins[$table]=$on;
        }

        /**
         * @return mixed
         */
        public function getEntity()
        {
            return $this->entity;
        }

        /**
         * @param mixed $entity
         * @return $this
         */
        public function setEntity($entity)
        {
            $this->entity = $entity;

            return $this;
        }


        /**
         * @return mixed
         */
        public function getCondition()
        {
            return $this->condition;
        }

        /**
         * @param mixed $condition
         * @return DbRequest
         */
        public function setCondition(array $condition)
        {
            $this->condition = $condition;

            return $this;
        }

        public function addCondition(array $condition)
        {
            $this->condition = array_merge($this->condition, $condition);
        }

        /**
         * @return mixed
         */
        public function getParameters()
        {
            return $this->parameters;
        }

        /**
         * @param mixed $parameters
         * @return DbRequest
         */
        public function setParameters($parameters)
        {
            $this->parameters = $parameters;

            return $this;
        }

        public function addParameter($p, $param)
        {
            $this->parameters[$p] = $param;
            return $this;
        }

        /**
         * @return mixed
         */
        public function getLimit()
        {
            return $this->limit;
        }

        /**
         * @param mixed $limit
         * @return DbRequest
         */
        public function setLimit($limit, $offset = null)
        {
            $this->limit[0] = $limit;
            $this->limit[1] = $offset;
            return $this;
        }

        /**
         * @return mixed
         */
        public function getOrder()
        {
            return $this->order;
        }

        /**
         * @param mixed $order
         * @return DbRequest
         */
        public function setOrder($order)
        {
            $this->order[] = $order;

            return $this;
        }

        /**
         * @return null
         */
        public function getAggr()
        {
            return $this->aggr;
        }

        /**
         * @param null $aggr
         * @return DbRequest
         */
        public function setAggr($aggr)
        {
            $this->aggr = $aggr;

            return $this;
        }

    }
