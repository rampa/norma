<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 18.5.2015
     * Time: 13:54
     * Package: nORMa
     * Licence: BSD
     */

    namespace Rampus\Norma;


    use Nette\Application\Application;
    use Nette\Caching\Cache;
    use Nette\DI\Container;
    use Tracy\Debugger;

    /**
     * Class EntityManager
     * @author  Jan Červeny <cerveny@redsoft.cz>
     * @package Rampus\Norma
     */
    class EntityManager
    {
        const ENTITY_NEW = 0;
        const ENTITY_PERSIST = 1;
        const ENTITY_LOADED = 3;
        const ENTITY_DELETED = 4;
        const ENTITY_NOT_FOUND = 8;
        const ENTITY_LOCKED = 16;
        const PROXY_NAMESPACE = 'Rampus\Norma\Proxy';
        /** @var Container */
        private $context;
        /** @var  DataProvider */
        private $dataProvider;
        /** @var \SplObjectStorage */
        private $entityChanged;
        /** @var \SplObjectStorage */
        private $collectionChanged;
        /** @var \SplObjectStorage */
        private $entityDeleted;
        /** @var \Nette\Caching\Cache */
        private $cache;
        /** @var array */
        private $entityRegistered = [];
        /** @var string */
        private $proxyPath;
        /** @var bool */
        private $trackChanges = true;
        /** @var bool */
        private $autoFlush = false;
        /** @var bool */
        private $prepareDB = false;

        /**
         * EntityManager constructor.
         * @param \Nette\DI\Container            $context
         * @param \Nette\Application\Application $app
         */
        public function __construct(Container $context)
        {
            $this->context = $context;
            $this->cache = new Cache($context->getService('cacheStorage'), 'norma');
            $this->entityChanged = new \SplObjectStorage();
            $this->collectionChanged = new \SplObjectStorage();
            $this->entityDeleted = new \SplObjectStorage();
            $this->proxyPath = $this->context->getParameters()['tempDir'] . '/cache/NormaProxy';
            if (!is_dir($this->proxyPath)) {
                mkdir($this->proxyPath);
            }
            $this->dataProvider = new DataProvider($this->context, $this);

        }

        /**
         * Reset all changes and restart entity manager
         * Mainly for testing
         * @internal
         */
        public function dataReset()
        {
            $this->entityRegistered = [];
            $this->dataProvider = new DataProvider($this->context, $this);
            $this->entityChanged = new \SplObjectStorage();
            $this->collectionChanged = new \SplObjectStorage();
            $this->entityDeleted = new \SplObjectStorage();

        }

        /**
         * Load proxy entity
         * @param $entity
         * @internal
         */
        private function tryLoad($entity)
        {
            $path = $this->proxyPath . DIRECTORY_SEPARATOR . pathinfo($entity)['basename'] . '.php';
            $this->cache->save('parse-in-progress', function () use ($entity, $path) {
                if (!Debugger::$productionMode) {
                    $this->cache->remove($entity);
                }
                $def = $this->cache->load($entity, function () use ($entity, $path) {
                    $a = pathinfo($entity);
                    $toParse = "\\Entity\\" . $entity;
                    if (!class_exists($toParse)) {
                        throw new EntityDefinitionNotFound("Entity definition '$toParse' not found");
                    }
                    $toParse = new $toParse;
                    $res = Parser::parse($toParse);
                    $file = fopen('nette.safe://' . $path, 'w');
                    $content = '<?php' . PHP_EOL . $res['proxy'];
                    fwrite($file, $content);
                    fclose($file);
                    $refl = new \ReflectionClass($toParse);
                    $this->cache->save($entity, $res['def'], [Cache::FILES => $refl->getFileName()]);

                    return $res['def'];
                });
                $this->dataProvider->addDefinition($entity, $def, $this->prepareDB);
            });

            include_once $path;
        }

        /**
         * Get one entity
         * @param string   $entity Class of entity
         * @param int|null $id     Id
         * @return \Rampus\Norma\BaseProxy
         */
        public function getOne($entity, $id = null)
        {
            $entityName = "\\Entity\\Proxy\\" . $entity;
            if (!class_exists($entityName) || (!$this->dataProvider->hasDefinition($entity))) {
                $this->tryLoad($entity);
            }
            if (is_array($id)) {
                $request = new DbRequest();
                $request->setEntity($entity)
                        ->setOperation('GET')
                        ->setLimit(1)
                        ->setCondition($id);
                $res = $this->dataProvider->execRequest($request);
                if ($res->getRows()) {
                    $row = $res->getData();

                    return $this->getOne($entity, array_shift($row)[$this->dataProvider->getDefinition($entity)['primaryKey']]);
                } else {
                    $id = null;
                }

            }
            if (!isset($this->entityRegistered[$this->hashEntityName($entity, $id)])) {
                /** @var BaseProxy $entity */
                $entity = new $entityName($id, $this, $this->dataProvider);
                if ($id) {
                    $this->registerEntity($entity, $id);
                    $this->dataProvider->addToQueue($entity->_getShortName(), $id);
                }

                return $entity;
            }


            return $this->entityRegistered[$this->hashEntityName($entity, $id)];
        }

        /**
         * Get collection of entities
         * @param $entity
         * @return \Rampus\Norma\EntityCollection
         */
        public function getAll($entity)
        {
            $this->tryLoad($entity);

            return new EntityCollection($entity, $this, $this->dataProvider);
        }

        /**
         * Register entity - internal
         * @internal
         * @param $entity
         * @param $id
         */
        public function registerEntity($entity, $id)
        {
            $this->entityRegistered[$this->hashEntityName($entity->_getShortName(), $id)] = $entity;
        }

        /**
         * @internal
         * @param $entity
         * @return bool
         */
        public function registerChange($entity)
        {
            if ($this->trackChanges) {
                if ($entity instanceof BaseProxy) {
                    $this->entityChanged->attach($entity);
                } else {
                    $this->collectionChanged->attach($entity);
                }
                if ($this->autoFlush) {
                    $this->flush();
                }
            }

            return $this->trackChanges;
        }

        /**
         * @internal
         * @param $entity
         */
        public function unregisterChange($entity)
        {
            $this->entityChanged->detach($entity);
        }

        /**
         * @internal
         * @param $entity
         */
        public function registerDelete($entity)
        {
            $this->entityDeleted->attach($entity);
            $this->unregisterChange($entity);
        }

        /**
         * Set flush after every change on/off
         * @param bool $val
         */
        public function setAutoFlush($val = true)
        {
            $app = $this->context->getService('application.application');
            $app->onPresenter[] = function ($pres) {
                $pres->onShutdown[] = callback($this, 'flush');
            };
        }

        /**
         * Flush changes to db
         */
        public function flush()
        {
            foreach ($this->entityDeleted as $val) {
                $request = new DbRequest();
                $request->setOperation('DEL')
                        ->setEntity($val->_getShortName())
                        ->setCondition([$val->getPrimaryKeyName() => $val->getPrimaryKey()]);
                $this->dataProvider
                    ->execRequest($request);
                unset($this->entityRegistered[$this->hashEntityName($val->_getShortName(), $val->getPrimaryKey())]);
            }
            $this->entityDeleted = new \SplObjectStorage();
            foreach ($this->collectionChanged as $val) {
                $val->save();
            }
            $this->collectionChanged = new \SplObjectStorage();
            foreach ($this->entityChanged as $val) {
                $val->save();
            }
            $this->entityChanged = new \SplObjectStorage();

        }

        /**
         * Start or stop monitoring changes of entities
         * Default 'On'
         * @param bool $val
         * @return $this
         */
        public function setTrackChange($val = true)
        {
            $this->trackChanges = (bool)$val;

            return $this;
        }

        /**
         * Get status of monitoring changes
         * @return bool
         */
        public function getTrackChange()
        {
            return $this->trackChanges;
        }

        /**
         * Get status of manipulating with db
         * @return boolean
         */
        public function isPrepareDB()
        {
            return $this->prepareDB;
        }

        /**
         * Manipulating with db structure on/off
         * @param boolean $prepareDB
         * @return EntityManager
         */
        public function setPrepareDB($prepareDB)
        {
            $this->prepareDB = $prepareDB;

            return $this;
        }


//-------------------------------------------------------------------------------------------- HELPERS -----------------
        /**
         * @internal
         * @param $entity
         * @param $id
         * @return string
         */
        private function hashEntityName($entity, $id)
        {
            if (is_object($entity)) {
                return get_class($entity) . '_' . $id;
            } else {
                return $entity . '_' . $id;
            }
        }

    }
