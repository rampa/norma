<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 7.5.2015
     * Time: 10:05
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Rampus\Norma;


    use Nette\Object;
    use Tracy\Debugger;
    use Tracy\IBarPanel;

    class NormaPanel extends Object implements IBarPanel
    {
        public $name = "nORMa";
        public static $count = 0;
        public static $totalTime = 0;
        public static $query = [];
        public $connection;

        /**
         * NormaPanel constructor.
         */
        public function __construct()
        {
            Debugger::getBar()
                    ->addPanel($this);
        }

        function getTab()
        {

            ob_start();
            require __DIR__ . '/PanelTab.phtml';

            return ob_get_clean();
        }

        function getPanel()
        {

            ob_start();
            require __DIR__ . '/Panel.phtml';

            return ob_get_clean();
        }

        public function logQuery(DbRequest $request, DbResult $res)
        {
            self::$totalTime += $res->getTime();
            self::$count++;
$sql=$res->getSql();
            $match=[];
            preg_match_all('/(:\d*)/',$sql,$match);
            $sql=preg_replace_callback("/:\d*/",function($match)use($request){
                return $request->getParameters()[$match[0]];
            },$sql);
            self::$query[] = ['request' => clone($request), 'time' => $res->getTime(), 'rows' => $res->getRows(), 'sql' => $sql];

        }

        public static function parseCondition($cond)
        {

            $res = "";
            foreach ($cond as $key => $val) {
                if (is_int($key)) {
                    $res = (strlen($res) ? '(' . $res . ') <span style="color:blue">OR</span> (' : null)
                        . self::parseCondition($val) . (strlen($res) ? ')' : null);
                } elseif (is_array($val)) {
                    $res .= (strlen($res) ? ' <span style="color:blue">AND</span> ' : null)
                        . $key . ' <span style="color:blue">IN</span> (' . implode(',', $val) . ')';
                } else {
                    $tmp = explode(' ', $key);
                    $key = $tmp[0];
                    unset($tmp[0]);
                    $operator = implode(' ', $tmp);
                    $operator = $operator ? strtoupper(' ' . $operator . ' ') : ' = ';
                    $res .= (strlen($res) ? ' <span style="color:blue">AND</span> ' : null);
                    $res .= '' . $key . $operator . (($val === '.EXP.') ? '' : $val);
                }
            }

            return $res;
        }


    }
